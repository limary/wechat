<?php

namespace Sinta\Wechat\OpenPlatform\Authorizer\Auth;

use Pimple\Container;
use Sinta\Wechat\OpenPlatform\Application;
use Sinta\Wechat\Kernel\AccessToken as BaseAccessToken;


class AccessToken extends BaseAccessToken
{
    protected $requestMethod = 'POST';

    protected $queryName = 'access_token';

    protected $tokenKey = 'component_access_token';

    //平台应用
    protected $component;

    /**
     * 认证token
     *
     * AccessToken constructor.
     * @param Container $app
     * @param Application $component
     */
    public function __construct(Container $app, Application $component)
    {
        parent::__construct($app);
        $this->component = $component;
    }


    protected function getCredentials(): array
    {
        return [
            'component_appid' => $this->component['config']['app_id'],
            'authorizer_appid' => $this->app['config']['app_id'],
            'authorizer_refresh_token' => $this->app['config']['refresh_token'],
        ];
    }


    public function getEndpoint(): string
    {
        return 'cgi-bin/component/api_authorizer_token?'.http_build_query([
            'component_access_token' => $this->component['access_token']->getToken()['component_access_token'],
        ]);
    }

}