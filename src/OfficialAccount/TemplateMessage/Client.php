<?php

namespace Sinta\Wechat\OfficialAccount\TemplateMessage;

use ReflectionClass;
use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;


class Client extends BaseClient
{
    const API_SEND = 'cgi-bin/message/template/send';

    protected $message = [
        'touser' => '',
        'template_id' => '',
        'url' => '',
        'data' => [],
    ];

    protected $required = ['touser', 'template_id'];


    /**
     * @param $industryOne
     * @param $industryTwo
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function setIndustry($industryOne, $industryTwo)
    {
        $params = [
            'industry_id1' => $industryOne,
            'industry_id2' => $industryTwo,
        ];
        return $this->httpPostJson('cgi-bin/template/api_set_industry', $params);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getIndustry()
    {
        return $this->httpPostJson('cgi-bin/template/get_industry');
    }

    /**
     * 添加模板
     *
     * @param $shortId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function addTemplate($shortId)
    {
        $params = ['template_id_short' => $shortId];
        return $this->httpPostJson('cgi-bin/template/api_add_template', $params);
    }


    /**
     * 获取私有模板
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getPrivateTemplates()
    {
        return $this->httpPostJson('cgi-bin/template/get_all_private_template');
    }

    /**
     * 删除私用模板
     *
     * @param $templateId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deletePrivateTemplate($templateId)
    {
        $params = ['template_id' => $templateId];
        return $this->httpPostJson('cgi-bin/template/del_private_template', $params);
    }

    /**
     * 发送模板消息
     *
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws InvalidArgumentException
     * @throws \ReflectionException
     */
    public function send($data = [])
    {
        $params = $this->formatMessage($data);
        $this->restoreMessage();
        return $this->httpPostJson(static::API_SEND, $params);
    }

    /**
     *
     *
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws InvalidArgumentException
     * @throws \ReflectionException
     */
    public function sendSubscription(array $data = [])
    {
        $params = $this->formatMessage($data);
        $this->restoreMessage();
        return $this->httpPostJson('cgi-bin/message/template/subscribe', $params);
    }


    /**
     * 格式消息
     *
     * @param array $data
     * @return array
     * @throws InvalidArgumentException
     */
    protected function formatMessage(array $data = [])
    {
        $params = array_merge($this->message, $data);
        foreach ($params as $key => $value) {
            if (in_array($key, $this->required, true) && empty($value) && empty($this->message[$key])) {
                throw new InvalidArgumentException(sprintf('Attribute "%s" can not be empty!', $key));
            }
            $params[$key] = empty($value) ? $this->message[$key] : $value;
        }
        $params['data'] = $this->formatData($params['data'] ?? []);
        return $params;
    }


    /**
     * 格式数据
     *
     * @param array $data
     * @return array
     */
    protected function formatData(array $data)
    {
        $formatted = [];
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (isset($value['value'])) {
                    $formatted[$key] = $value;
                    continue;
                }
                if (count($value) >= 2) {
                    $value = [
                        'value' => $value[0],
                        'color' => $value[1],
                    ];
                }
            } else {
                $value = [
                    'value' => strval($value),
                ];
            }
            $formatted[$key] = $value;
        }
        return $formatted;
    }


    /**
     * Restore Message
     *
     * @throws \ReflectionException
     */
    protected function restoreMessage()
    {
        $this->message = (new ReflectionClass(static::class))->getDefaultProperties()['message'];
    }


}