<?php

namespace Sinta\Wechat\Payment\Base;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 基础服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\Payment\Base
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['base'] = function($app){
            return new Client($app);
        };
    }
}