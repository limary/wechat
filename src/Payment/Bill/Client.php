<?php

namespace Sinta\Wechat\Payment\Bill;

use Sinta\Wechat\Kernel\Http\StreamResponse;
use Sinta\Wechat\Payment\Kernel\BaseClient;


/**
 * 下载对账单
 *
 * Class Client
 * @package Sinta\Wechat\Payment\Bill
 */
class Client extends BaseClient
{
    public function download(string $date,string $type = 'ALL',array $optional = [])
    {
        $params = [
            'appid' => $this->app['config']->app_id,
            'bill_date' => $date,
            'bill_type' => $type,
        ] + $optional;

        $response = $this->requestRaw($this->wrap('pay/downloadbill'), $params);

        if(0 === strpos($response->getBody()->getContents(),'<xml>') ){
            return $this->resolveResponse($response, $this->app['config']->get('response_type', 'array'));
        }

        return StreamResponse::buildFromPsrResponse($response);
    }
}