<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\Aggregate;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Sinta\Wechat\OpenPlatform\Authorizer\Aggregate\Account\Client;


/**
 *  聚合服务
 *
 * Class AggregateServiceProvider
 */
class AggregateServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['account'] = function ($app) {
            return new Client($app);
        };
    }
}