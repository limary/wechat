<?php


namespace Sinta\Wechat\Tests\Kernel\Messages;

use Sinta\Wechat\Kernel\Messages\Raw;
use Sinta\Wechat\Tests\TestCase;

class RawTest extends TestCase
{
    public function testBasicFeatures()
    {
        $content = '<xml><foo>foo</foo></xml>';
        $raw = new Raw($content);

        $this->assertSame($content, $raw->content);

        $this->assertSame($content, strval($raw));
    }
}
