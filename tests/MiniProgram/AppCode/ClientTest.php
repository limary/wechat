<?php
namespace Sinta\Wechat\Tests\MiniProgram\AppCode;

use Sinta\Wechat\Kernel\Http\StreamResponse;
use Sinta\Wechat\MiniProgram\AppCode\Client;
use Sinta\Wechat\Tests\TestCase;

class ClientTest extends TestCase
{
    protected $mockStream;

    public function setUp()
    {
        parent::setUp();

        $this->mockStream = new \Sinta\Wechat\Kernel\Http\Response();
    }

    public function testGetAppCode()
    {
        $client = $this->mockApiClient(Client::class);

        $client->expects()->requestRaw('wxa/getwxacode', 'POST', ['json' => [
            'path' => 'foo-path',
            'width' => 430,
        ]])->andReturn($this->mockStream)->once();

        $this->assertInstanceOf(StreamResponse::class, $client->get('foo-path', [
            'width' => 430,
        ]));
    }

    public function testGetAppCodeUnlimit()
    {
        $client = $this->mockApiClient(Client::class);

        $client->expects()->requestRaw('wxa/getwxacodeunlimit', 'POST', ['json' => [
            'scene' => 'scene',
            'page' => '/app/pages/hello',
        ]])->andReturn($this->mockStream)->once();

        $this->assertInstanceOf(StreamResponse::class, $client->getUnlimit('scene', [
            'page' => '/app/pages/hello',
        ]));
    }

    public function testCreateQrCode()
    {
        $client = $this->mockApiClient(Client::class);

        $client->expects()->requestRaw('cgi-bin/wxaapp/createwxaqrcode', 'POST', ['json' => [
            'path' => 'foo-path',
            'width' => null,
        ]])->andReturn($this->mockStream)->once();

        $this->assertInstanceOf(StreamResponse::class, $client->getQrCode('foo-path'));
    }
}
