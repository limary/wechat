<?php
namespace Sinta\Wechat\OfficialAccount\CustomerService;


use Sinta\Wechat\Kernel\Exceptions\RuntimeException;
use Sinta\Wechat\Kernel\Messages\Raw as RawMessage;
use Sinta\Wechat\Kernel\Messages\Text;

/**
 *会话消息创建
 *
 * Class Messenger
 * @package Sinta\Wechat\OfficialAccount\CustomerService
 */
class Messenger
{

    /**
     * 消息体
     * @var \Sinta\Wechat\Kernel\Messages\Message
     */
    protected $message;

    /**
     * 消息发送的地址，openid
     *
     * @var string
     */
    protected $to;

    /**
     * 消息发送者
     * @var string
     */
    protected $account;


    /**
     * 客服实例
     *
     * @var Client
     */
    protected $client;


    /**
     *
     * Messenger constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    public function message($message)
    {
        if (is_string($message)) {
            $message = new Text($message);
        }

        $this->message = $message;

        return $this;
    }

    /**
     * 设置发送者
     *
     * @param string $account
     * @return $this
     */
    public function by(string $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     *设置发送者
     *
     * @param string $account
     * @return Messenger
     */
    public function from(string $account)
    {
        return $this->by($account);
    }

    /**
     * 设置接收者
     *
     * @param $openid
     * @return $this
     */
    public function to($openid)
    {
        $this->to = $openid;

        return $this;
    }


    /**
     * 发送
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws RuntimeException
     */
    public function send()
    {
        if (empty($this->message)) {
            throw new RuntimeException('No message to send.');
        }

        if ($this->message instanceof RawMessage) {
            $message = json_decode($this->message->get('content'), true);
        } else {
            $prepends = [
                'touser' => $this->to,
            ];
            if ($this->account) {
                $prepends['customservice'] = ['kf_account' => $this->account];
            }
            $message = $this->message->transformForJsonRequest($prepends);
        }

        return $this->client->send($message);
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        return null;
    }

}