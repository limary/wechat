<?php

namespace Sinta\Wechat\Payment\Kernel\Exceptions;

use Sinta\Wechat\Kernel\Exceptions\Exception;

class InvalidSignException extends Exception
{

}