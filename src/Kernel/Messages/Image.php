<?php
namespace Sinta\Wechat\Kernel\Messages;

/**
 * 图片
 *
 * Class Image
 * @package Sinta\Wechat\Kernel\Messages
 */
class Image extends Media
{
    protected $type = 'image';
}