<?php
namespace Sinta\Wechat\Work\Message;


use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;
use Sinta\Wechat\Kernel\Exceptions\RuntimeException;
use Sinta\Wechat\Kernel\Messages\Message;
use Sinta\Wechat\Kernel\Messages\Text;

/**
 * 通信器
 *
 * Class Messenger
 * @package Sinta\Wechat\Work\Message
 */
class Messenger
{
    /**
     * 消息
     *
     * @var \Sinta\Wechat\Kernel\Messages\Message
     */
    protected $message;

    /**
     * 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
     *
     * @var array
     */
    protected $to = ['touser' => '@all'];

    /**
     * @var int 企业应用的id，整型。可在应用的设置页面查看
     */
    protected $agentId;

    protected $secretive = false;

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    /**
     * 消息设置
     *
     * @param $message
     * @return $this
     * @throws InvalidArgumentException
     */
    public function message($message)
    {
        if(is_string($message) || is_numeric($message)){
            $message = new Text($message);
        }

        if(!($message instanceof Message)){
            throw new InvalidArgumentException("Invalid message.");
        }

        $this->message = $message;
        return $this;
    }

    /**
     * 	企业应用的id设置
     *
     * @param int $agentId
     * @return $this
     */
    public function ofAgent(int $agentId)
    {
        $this->agentId = $agentId;
        return $this;
    }

    /**
     * 发送给用户
     *
     * @param $userIds
     * @return Messenger
     */
    public function toUser($userIds)
    {
        return $this->setRecipients($userIds, 'touser');
    }

    public function toParty($partyIds)
    {
        return $this->setRecipients($partyIds, 'toparty');
    }

    public function toTag($tagIds)
    {
        return $this->setRecipients($tagIds, 'totag');
    }

    public function secretive()
    {
        $this->secretive = true;
        return $this;
    }


    protected function setRecipients($ids, string $key): Messenger
    {
        if (is_array($ids)) {
            $ids = implode('|', $ids);
        }
        $this->to = [$key => $ids];
        return $this;
    }

    /**
     * 发送消息
     *
     * @param null $message
     * @return \Psr\Http\Message\ResponseInterface
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function send($message = null)
    {
        if ($message) {
            $this->message($message);
        }
        if (empty($this->message)) {
            throw new RuntimeException('No message to send.');
        }
        if (is_null($this->agentId)) {
            throw new RuntimeException('No agentid specified.');
        }
        $message = $this->message->transformForJsonRequest(array_merge([
            'agentid' => $this->agentId,
            'safe' => intval($this->secretive),
        ], $this->to));
        return $this->client->send($message);
    }

    /**
     * 获取属性
     *
     * @param $property
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        throw new InvalidArgumentException(sprintf('No property named "%s"', $property));
    }

}