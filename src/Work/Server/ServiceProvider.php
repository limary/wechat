<?php
namespace Sinta\Wechat\Work\Server;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Sinta\Wechat\Kernel\Encryptor;
use Sinta\Wechat\Work\Server\Handlers\EchoStrHandler;


class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        !isset($app['encryptor']) && $app['encryptor'] = function ($app) {
            return new Encryptor(
                $app['config']['corp_id'],
                $app['config']['token'],
                $app['config']['aes_key']
            );
        };

        !isset($app['server']) && $app['server'] = function ($app) {
            $guard = new Guard($app);
            $guard->push(new EchoStrHandler($app));

            return $guard;
        };
    }
}