<?php
namespace Sinta\Wechat\OpenPlatform\Auth;

use Sinta\Wechat\Kernel\AccessToken as BaseAccessToken;

/**
 * Class AccessToken
 * @package Sinta\Wechat\OpenPlatform\Auth
 */
class AccessToken extends BaseAccessToken
{
    protected $requestMethod = 'POST';

    protected $tokenKey = 'component_access_token';

    protected $endpointToGetToken = 'cgi-bin/component/api_component_token';

    protected function getCredentials(): array
    {
        return [
            'component_appid' => $this->app['config']['app_id'],
            'component_appsecret' => $this->app['config']['secret'],
            'component_verify_ticket' => $this->app['verify_ticket']->getTicket(),
        ];
    }
}