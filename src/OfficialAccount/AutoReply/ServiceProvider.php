<?php
namespace Sinta\Wechat\OfficialAccount\AutoReply;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 自动应答服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['auto_reply'] = function($app){
            return new Client($app);
        };
    }
}