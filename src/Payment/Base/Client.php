<?php

namespace Sinta\Wechat\Payment\Base;

use Sinta\Wechat\Payment\Kernel\BaseClient;

class Client extends BaseClient
{
    /**
     * 提交刷卡支付API
     *
     *
     * @param array $params
     * @see https://pay.weixin.qq.com/wiki/doc/api/micropay_sl_jw.php?chapter=9_10&index=1
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function pay(array $params)
    {
        $params['appid'] = $this->app['config']->app_id;

        return $this->request($this->wrap('pay/micropay'), $params);
    }

    /**
     * 授权码查询openid
     *
     * @param string $authCode
     * @see https://pay.weixin.qq.com/wiki/doc/api/micropay.php?chapter=9_13&index=9
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function authCodeToOpenid(string $authCode)
    {
        return $this->request('tools/authcodetoopenid', [
            'appid' => $this->app['config']->app_id,
            'auth_code' => $authCode
        ]);
    }
}