<?php
namespace Sinta\Wechat\Kernel\Messages;

/**
 * 短视频
 *
 * Class ShortVideo
 * @package Sinta\Wechat\Kernel\Messages
 */
class ShortVideo extends Video
{
    protected $type = 'shortvideo';
}