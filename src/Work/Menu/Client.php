<?php
namespace Sinta\Wechat\Work\Menu;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * Class Client
 * @see http://qydev.weixin.qq.com/wiki/index.php?title=%E5%88%9B%E5%BB%BA%E5%BA%94%E7%94%A8%E8%8F%9C%E5%8D%95
 * @package Sinta\Wechat\Work\Menu
 */
class Client extends BaseClient
{
    /**
     * Get menu.
     *
     * 目前自定义菜单最多包括3个一级菜单，每个一级菜单最多包含5个二级菜单。一级菜单最多4个汉字，二级菜单最多8个汉字，多出来的部分将会以“...”代替。
     * 请注意，创建自定义菜单后，由于微信客户端缓存，需要24小时微信客户端才会展现出来。建议测试时可以尝试取消关注企业号后再次关注，则可以看到创建后的效果。
     *
     * @return mixed
     */
    public function get()
    {
        return $this->httpGet('cgi-bin/menu/get', ['agentid' => $this->app['config']['agent_id']]);
    }

    /**
     * Create menu for the given agent.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->httpPostJson('cgi-bin/menu/create', $data, ['agentid' => $this->app['config']['agent_id']]);
    }

    /**
     * Delete menu.
     *
     * @return mixed
     */
    public function delete()
    {
        return $this->httpGet('cgi-bin/menu/delete', ['agentid' => $this->app['config']['agent_id']]);
    }
}
