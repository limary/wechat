<?php
namespace Sinta\Wechat\OfficialAccount\Server\Handlers;


use Sinta\Wechat\Kernel\Decorators\FinallyResult;
use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\Kernel\Contracts\EventHandlerInterface;

class EchoStrHandler implements EventHandlerInterface
{

    protected $app;

    /**
     * EchoStrHandler constructor.
     *
     * @param ServiceContainer $app
     */
    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;
    }

    /**
     * @param array $payload
     *
     * @return FinallyResult|null
     */
    public function handle($payload = null)
    {
        if ($str = $this->app['request']->get('echostr')) {
            return new FinallyResult($str);
        }
    }
}