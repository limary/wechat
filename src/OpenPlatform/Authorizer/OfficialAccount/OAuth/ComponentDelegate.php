<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\OfficialAccount\OAuth;

use Sinta\Wechat\OpenPlatform\Application;
use Sinta\Socialite\WeChatComponentInterface;

/**
 * Class ComponentDelegate
 */
class ComponentDelegate implements WeChatComponentInterface
{

    protected $app;

    /**
     * ComponentDelegate constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return string
     */
    public function getAppId()
    {
        return $this->app['config']['app_id'];
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->app['access_token']->getToken()['component_access_token'];
    }
}
