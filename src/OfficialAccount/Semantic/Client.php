<?php
namespace Sinta\Wechat\OfficialAccount\Semantic;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    public function query(string $keyword, string $categories, array $optional = [])
    {
        $params = [
            'query' => $keyword,
            'category' => $categories,
            'appid' => $this->app['config']['app_id'],
        ];

        return $this->httpPostJson('semantic/semproxy/search', array_merge($params, $optional));
    }
}