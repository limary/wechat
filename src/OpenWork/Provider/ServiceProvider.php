<?php
namespace Sinta\Wechat\OpenWork\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider
{
    protected $app;
    /**
     * @param Container $app
     */
    public function register(Container $app)
    {
        $this->app = $app;
        isset($app['provider']) || $app['provider'] = function ($app) {
            return new Client($app);
        };
    }
}