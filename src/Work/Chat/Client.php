<?php
namespace Sinta\Wechat\Work\Chat;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    public function get(string $chatId)
    {
        return $this->httpGet('cgi-bin/appchat/get', ['chatid' => $chatId]);
    }

    public function create(array $data)
    {
        return $this->httpPostJson('cgi-bin/appchat/create', $data);
    }

    public function update(string $chatId, array $data)
    {
        return $this->httpPostJson('cgi-bin/appchat/update', array_merge(['chatid' => $chatId], $data));
    }


    public function send(array $message)
    {
        return $this->httpPostJson('cgi-bin/appchat/send', $message);
    }

}