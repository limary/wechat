<?php
namespace Sinta\Wechat\MiniProgram\DataCube;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 用户数据分析
 *
 * Class Client
 * @package Sinta\Wechat\MiniProgram\DataCube
 * @https://mp.weixin.qq.com/debug/wxadoc/dev/api/analysis-visit.html?t=2017712
 */
class Client extends BaseClient
{
    /**
     * 用户访问小程序的详细数据可从访问分析中获取，
     * 概况中提供累计用户数等部分指标数据。
     *
     * @param string $from  开始日期 20170313
     * @param string $to  结束日期，   20170313"限定查询1天数据，end_date允许设置的最大值为昨日
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function summaryTrend(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappiddailysummarytrend', $from, $to);
    }

    /**
     * 日趋势
     *
     * @param string $from 开始日期  20170313
     * @param string $to 结束日期，20170313 限定查询1天数据，end_date允许设置的最大值为昨日
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function dailyVisitTrend(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappiddailyvisittrend', $from, $to);
    }

    /**
     * 周趋势
     *
     * 时间必须按照自然周的方式输入： 如：20170306(周一), 20170312(周日)
     * @param string $from  开始日期，为周一日期
     * @param string $to    结束日期，为周日日期，限定查询一周数据
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function weeklyVisitTrend(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappidweeklyvisittrend', $from, $to);
    }

    /**
     * 月趋势
     *
     * 如：20170201(月初), 20170228(月末)
     * @param string $from   开始日期，为自然月第一天
     * @param string $to     结束日期，为自然月最后一天，限定查询一个月数据
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function monthlyVisitTrend(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappidmonthlyvisittrend', $from, $to);
    }


    /**
     * 访问分布
     *
     * @param string $from
     * @param string $to  结束日期，限定查询1天数据，end_date允许设置的最大值为昨日
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function visitDistribution(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappidvisitdistribution', $from, $to);
    }

    /**
     * 日留存
     *
     * @param string $from
     * @param string $to  结束日期，限定查询1天数据，end_date允许设置的最大值为昨日
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function dailyRetainInfo(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappiddailyretaininfo', $from, $to);
    }


    /**
     * 周留存
     *
     * @param string $from
     * @param string $to
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function weeklyRetainInfo(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappidweeklyretaininfo', $from, $to);
    }

    /**
     * 月留存
     *
     * @param string $from
     * @param string $to  结束日期，为自然月最后一天，限定查询一个月数据
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function monthlyRetainInfo(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappidmonthlyretaininfo', $from, $to);
    }

    /**
     * 访问页面
     *
     * @param string $from
     * @param string $to
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function visitPage(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappidvisitpage', $from, $to);
    }

    /**
     * 用户画像
     *
     * 获取小程序新增或活跃用户的画像分布数据。时间范围支持昨天、最近7天、最近30天。其中，
     * 新增用户数为时间范围内首次访问小程序的去重用户数，活跃用户数为时间范围内访问过小程序的去重用户数。
     * 画像属性包括用户年龄、性别、省份、城市、终端类型、机型。
     *
     * @param string $from
     * @param string $to
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function userPortrait(string $from, string $to)
    {
        return $this->query('datacube/getweanalysisappiduserportrait', $from, $to);
    }

    protected function query(string $api, string $from, string $to)
    {
        $params = [
            'begin_date' => $from,
            'end_date' => $to,
        ];
        return $this->httpPostJson($api, $params);
    }

}