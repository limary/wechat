<?php
namespace Sinta\Wechat\MiniProgram\NearbyPoi;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 附近小程序
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\MiniProgram\NearbyPoi
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['nearby_poi'] = function ($app) {
            return new Client($app);
        };
    }
}