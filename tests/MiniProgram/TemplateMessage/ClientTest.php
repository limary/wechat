<?php
namespace Sinta\Wechat\Tests\MiniProgram\TemplateMessage;

use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;
use Sinta\Wechat\MiniProgram\TemplateMessage\Client;
use Sinta\Wechat\Tests\TestCase;

class ClientTest extends TestCase
{
    public function testSend()
    {
        $client = $this->mockApiClient(Client::class)->makePartial();

        // without touser
        try {
            $client->send();
        } catch (\Exception $e) {
            $this->assertInstanceOf(InvalidArgumentException::class, $e);
            $this->assertSame('Attribute "touser" can not be empty!', $e->getMessage());
        }

        // without template_id
        try {
            $client->send(['touser' => 'mock-openid']);
        } catch (\Exception $e) {
            $this->assertInstanceOf(InvalidArgumentException::class, $e);
            $this->assertSame('Attribute "template_id" can not be empty!', $e->getMessage());
        }

        $client->expects()->httpPostJson('cgi-bin/message/wxopen/template/send', [
            'touser' => 'mock-openid',
            'template_id' => 'mock-template_id',
            'page' => '',
            'form_id' => 'mock-form_id',
            'emphasis_keyword' => '',
            'data' => [],
        ])->andReturn('mock-result')->once();
        $this->assertSame('mock-result', $client->send(['touser' => 'mock-openid', 'template_id' => 'mock-template_id', 'form_id' => 'mock-form_id']));
    }
}
