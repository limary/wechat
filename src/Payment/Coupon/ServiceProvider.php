<?php

namespace Sinta\Wechat\Payment\Coupon;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 代金券服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\Payment\Coupon
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['coupon'] = function ($app) {
            return new Client($app);
        };
    }
}