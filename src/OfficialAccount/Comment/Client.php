<?php
namespace Sinta\Wechat\OfficialAccount\Comment;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 文章评论
 *
 * Class Client
 * @package Sinta\Wechat\OfficialAccount\Comment
 */
class Client extends BaseClient
{
    /**
     * 打开已群发文章评论（新增接口）
     *
     * @param string $msgId
     * @param int|null $index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function open(string $msgId, int $index = null)
    {
        $params = [
            'msg_data_id' => $msgId,
            'index' => $index,
        ];

        return $this->httpPostJson('cgi-bin/comment/open', $params);
    }

    /**
     * 关闭已群发文章评论（新增接口）
     *
     * @param string $msgId
     * @param int|null $index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function close(string $msgId, int $index = null)
    {
        $params = [
            'msg_data_id' => $msgId,
            'index' => $index,
        ];

        return $this->httpPostJson('cgi-bin/comment/close', $params);
    }

    /**
     * 查看指定文章的评论数据（新增接口）
     *
     * @param string $msgId
     * @param int $index
     * @param int $begin
     * @param int $count
     * @param int $type
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list(string $msgId, int $index, int $begin, int $count, int $type = 0)
    {
        $params = [
            'msg_data_id' => $msgId,
            'index' => $index,
            'begin' => $begin,
            'count' => $count,
            'type' => $type,
        ];

        return $this->httpPostJson('cgi-bin/comment/list', $params);
    }

    /**
     * 将评论标记精选（新增接口）
     *
     * @param string $msgId
     * @param int $index
     * @param int $commentId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function markElect(string $msgId, int $index, int $commentId)
    {
        $params = [
            'msg_data_id' => $msgId,
            'index' => $index,
            'user_comment_id' => $commentId,
        ];

        return $this->httpPostJson('cgi-bin/comment/markelect', $params);
    }

    /**
     * 将评论取消精选
     *
     * @param string $msgId
     * @param int $index
     * @param int $commentId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function unmarkElect(string $msgId, int $index, int $commentId)
    {
        $params = [
            'msg_data_id' => $msgId,
            'index' => $index,
            'user_comment_id' => $commentId,
        ];

        return $this->httpPostJson('cgi-bin/comment/unmarkelect', $params);
    }


    /**
     * 删除评论
     *
     * @param string $msgId
     * @param int $index
     * @param int $commentId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(string $msgId, int $index, int $commentId)
    {
        $params = [
            'msg_data_id' => $msgId,
            'index' => $index,
            'user_comment_id' => $commentId,
        ];

        return $this->httpPostJson('cgi-bin/comment/delete', $params);
    }


    /**
     * 回复评论
     *
     * @param string $msgId
     * @param int $index
     * @param int $commentId
     * @param string $content
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function reply(string $msgId, int $index, int $commentId, string $content)
    {
        $params = [
            'msg_data_id' => $msgId,
            'index' => $index,
            'user_comment_id' => $commentId,
            'content' => $content,
        ];

        return $this->httpPostJson('cgi-bin/comment/reply/add', $params);
    }

    /**
     * 删除回复
     *
     * @param string $msgId
     * @param int $index
     * @param int $commentId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deleteReply(string $msgId, int $index, int $commentId)
    {
        $params = [
            'msg_data_id' => $msgId,
            'index' => $index,
            'user_comment_id' => $commentId,
        ];

        return $this->httpPostJson('cgi-bin/comment/reply/delete', $params);
    }
}