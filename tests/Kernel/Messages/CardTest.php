<?php

namespace Sinta\Wechat\Test\Kernel\Messages;

use Sinta\Wechat\Kernel\Messages\Card;
use Sinta\Wechat\Tests\TestCase;

class CardTest extends TestCase
{
    public function testBasicFeatures()
    {
        $card = new Card('mock-card-id');

        $this->assertSame('mock-card-id', $card->card_id);
    }
}
