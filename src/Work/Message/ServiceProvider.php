<?php
namespace Sinta\Wechat\Work\Message;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['message'] = function ($app){
            return new Client($app);
        };

        $app['messenger'] = function($app){
            return (new Messenger($app['message']))->ofAgent($app['config']['agent_id']);
        };
    }
}