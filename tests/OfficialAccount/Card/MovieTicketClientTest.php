<?php

namespace Sinta\Wechat\Tests\OfficialAccount\Card;

use Sinta\Wechat\OfficialAccount\Card\MovieTicketClient;
use Sinta\Wechat\Tests\TestCase;

class MovieTicketClientTest extends TestCase
{
    public function testUpdateUser()
    {
        $client = $this->mockApiClient(MovieTicketClient::class);

        $client->expects()->httpPostJson('card/movieticket/updateuser', ['foo' => 'bar'])->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->updateUser(['foo' => 'bar']));
    }
}
