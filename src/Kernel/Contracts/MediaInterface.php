<?php
namespace Sinta\Wechat\Kernel\Contracts;

/**
 *
 * Interface IMedia
 * @package Sinta\Wechat\Kernel\Contract
 */
interface  MediaInterface extends MessageInterface
{
    public function getMediaId(): string;
}