<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Domain;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 域名管理服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['domain'] = function ($app){
            return new Client($app);
        };
    }
}