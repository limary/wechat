<?php
namespace Sinta\Wechat\MiniProgram\NearbyPoi;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;

class Client extends BaseClient
{
    /**
     * 查看地点列表
     *
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/getNearbyPoiList.html
     * @param int $page
     * @param int $pageRows
     * @return mixed
     */
    public function list(int $page, int $pageRows)
    {
        return $this->httpGet('wxa/getnearbypoilist', [
            'page' => $page,
            'page_rows' => $pageRows,
        ]);
    }

    /**
     * 添加地点
     *
     * @param string $name
     * @param string $credential
     * @param string $address
     * @param string|null $proofMaterial
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function add(string $name, string $credential, string $address, string $proofMaterial = null)
    {
        return $this->httpPostJson('wxa/addnearbypoi', [
            'related_name' => $name,
            'related_credential' => $credential,
            'related_address' => $address,
            'related_proof_material' => $proofMaterial,
        ]);
    }

    /**
     * 删除地点
     *
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/deleteNearbyPoi.html
     *
     * @param string $poiId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(string $poiId)
    {
        return $this->httpPostJson('wxa/delnearbypoi', [
            'poi_id' => $poiId,
        ]);
    }

    /**
     * 展示/取消展示附近小程序
     *
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/setNearbyPoiShowStatus.html
     * @param string $poiId
     * @param int $status
     * @return \Psr\Http\Message\ResponseInterface
     * @throws InvalidArgumentException
     */
    public function setVisibility(string $poiId, int $status)
    {
        if (!in_array($status, [0, 1], true)) {
            throw new InvalidArgumentException('status should be 0 or 1.');
        }
        return $this->httpPostJson('wxa/setnearbypoishowstatus', [
            'poi_id' => $poiId,
            'status' => $status,
        ]);
    }
}