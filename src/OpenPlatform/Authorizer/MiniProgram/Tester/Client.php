<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Tester;

use Sinta\Wechat\Kernel\Client as BaseClient;


class Client extends BaseClient
{
    /**
     * 绑定小程序体验者.
     *
     * @param string $wechatId
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function bind(string $wechatId)
    {
        return $this->httpPostJson('wxa/bind_tester', [
            'wechatid' => $wechatId,
        ]);
    }

    /**
     * 解绑小程序体验者
     *
     * @param string $wechatId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function unbind(string $wechatId)
    {
        return $this->httpPostJson('wxa/unbind_tester', [
            'wechatid' => $wechatId,
        ]);
    }

    /**
     * 获取体验者列表
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list()
    {
        return $this->httpPostJson('wxa/memberauth', [
            'action' => 'get_experiencer',
        ]);
    }
}