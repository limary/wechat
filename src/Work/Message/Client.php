<?php
namespace Sinta\Wechat\Work\Message;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Messages\Message;

class Client extends BaseClient
{
    /**
     * @param string| \Sinta\Wechat\Kernel\Messages\Message $message
     * @return $this
     */
    public function message($message)
    {
        return (new Messenger($this))->message($message);
    }

    /**
     * 发送消息
     *
     * 消息型应用支持文本、图片、语音、视频、文件、图文等消息类型。主页型应用只支持文本消息类型，且文本长度不超过20个字。
     *
     * @param array $message
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function send(array $message)
    {
        return $this->httpPostJson('cgi-bin/message/send',$message);
    }
}