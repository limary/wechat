<?php
namespace Sinta\Wechat\OfficialAccount\AutoReply;

use Sinta\Wechat\Kernel\Client as BaseClient;



class Client extends BaseClient
{
    /**
     * 当前自动应答设置
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function current()
    {
        return $this->httpGet('cgi-bin/get_current_autoreply_info');
    }
}