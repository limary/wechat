<?php

namespace Sinta\Wechat\Tests\OfficialAccount\Card;

use Sinta\Wechat\OfficialAccount\Card\GeneralCardClient;
use Sinta\Wechat\Tests\TestCase;

class GeneralCardClientTest extends TestCase
{
    public function testActivate()
    {
        $client = $this->mockApiClient(GeneralCardClient::class);

        $params = [
            'foo' => 'bar',
        ];
        $client->expects()->httpPostJson('card/generalcard/activate', $params)->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->activate($params));
    }

    public function testDeactivate()
    {
        $client = $this->mockApiClient(GeneralCardClient::class);

        $params = [
            'card_id' => 'mock-card-id',
            'code' => 'bar',
        ];
        $client->expects()->httpPostJson('card/generalcard/unactivate', $params)->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->deactivate('mock-card-id', 'bar'));
    }

    public function testUpdateUser()
    {
        $client = $this->mockApiClient(GeneralCardClient::class);

        $client->expects()->httpPostJson('card/generalcard/updateuser', ['foo' => 'bar'])->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->updateUser(['foo' => 'bar']));
    }
}
