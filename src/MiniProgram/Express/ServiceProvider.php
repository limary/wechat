<?php
namespace Sinta\Wechat\MiniProgram\Express;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 物流助手
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\MiniProgram\Express
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['express'] = function ($app) {
            return new Client($app);
        };
    }
}