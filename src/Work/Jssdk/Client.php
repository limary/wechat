<?php
namespace Sinta\Wechat\Work\Jssdk;

use Sinta\Wechat\BasicService\Jssdk\Client as BaseClient;

class Client extends BaseClient
{
    const API_GET_TICKET = 'https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket';

    protected function getAppId()
    {
        return $this->app['config']->get('corp_id');
    }
}