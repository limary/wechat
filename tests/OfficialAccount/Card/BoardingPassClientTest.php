<?php
namespace Sinta\Wechat\Tests\OfficialAccount\Card;

use Sinta\Wechat\OfficialAccount\Card\BoardingPassClient;
use Sinta\Wechat\Tests\TestCase;

class BoardingPassClientTest extends TestCase
{
    public function testCheckin()
    {
        $client = $this->mockApiClient(BoardingPassClient::class);

        $params = [
            'foo' => 'bar',
        ];
        $client->expects()->httpPostJson('card/boardingpass/checkin', $params)->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->checkin($params));
    }
}
