<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram;

use Sinta\Wechat\MiniProgram\Application as MiniProgram;
use Sinta\Wechat\OpenPlatform\Authorizer\Aggregate\AggregateServiceProvider;

/**
 * 授权的小程序应用
 *
 * Class Application
 */
class Application extends MiniProgram
{
    /**
     * 创建授权的小程序应用
     *
     * 添加新的服务
     *
     * Application constructor.
     * @param array $config
     * @param array $prepends
     */
    public function __construct(array $config = [], array $prepends = [])
    {
        parent::__construct($config, $prepends);

        $providers = [
            AggregateServiceProvider::class,
            Code\ServiceProvider::class,
            Domain\ServiceProvider::class,
            Account\ServiceProvider::class,
            Setting\ServiceProvider::class,
            Tester\ServiceProvider::class,
        ];

        foreach ($providers as $provider) {
            $this->register(new $provider());
        }
    }
}