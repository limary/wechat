<?php
namespace Sinta\Wechat\OfficialAccount\Card;

use Sinta\Wechat\BasicService\Jssdk\Client as Jssdk;
use Sinta\Wechat\Kernel\Support\Arr;
use function Sinta\Wechat\Kernel\Support\str_random;

class JssdkClient extends Jssdk
{
    public function getTicket(bool $refresh = false, string $type = 'wx_card'): array
    {
        return parent::getTicket($refresh, $type);
    }

    /**
     * 微信卡券：JSAPI 卡券发放.
     *
     * @param array $cards
     * @return string
     */
    public function assign(array $cards)
    {
        return json_encode(array_map(function ($card) {
            return $this->attachExtension($card['card_id'], $card);
        }, $cards));
    }


    public function attachExtension($cardId, array $extension = [])
    {
        $timestamp = time();
        $nonce = str_random(6);
        $ticket = $this->getTicket()['ticket'];
        $ext = array_merge(['timestamp' => $timestamp, 'nonce_str' => $nonce], Arr::only(
            $extension,
            ['code', 'openid', 'outer_id', 'balance', 'fixed_begintimestamp', 'outer_str']
        ));
        $ext['signature'] = $this->dictionaryOrderSignature($ticket, $timestamp, $cardId, $ext['code'] ?? '', $ext['openid'] ?? '', $nonce);
        return [
            'cardId' => $cardId,
            'cardExt' => json_encode($ext),
        ];
    }
}