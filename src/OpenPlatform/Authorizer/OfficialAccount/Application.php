<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\OfficialAccount;

use Sinta\Wechat\OfficialAccount\Application as OfficialAccount;
use Sinta\Wechat\OpenPlatform\Authorizer\Aggregate\AggregateServiceProvider;

/**
 * 授权的认证的公众号应用
 *
 * Class Application
 * @package Sinta\Wechat\OpenPlatform\Authorizer\OfficialAccount
 */
class Application extends OfficialAccount
{
    public function __construct(array $config = [], array $prepends = [])
    {
        parent::__construct($config, $prepends);

        $providers = [
            AggregateServiceProvider::class,
            MiniProgram\ServiceProvider::class,
        ];

        foreach ($providers as $provider) {
            $this->register(new $provider());
        }
    }
}