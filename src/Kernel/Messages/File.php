<?php
namespace Sinta\Wechat\Kernel\Messages;

/**
 * 文件消息
 *
 * Class File
 * @package Sinta\Wechat\Kernel\Messages
 */
class File extends Media
{
    protected $type = 'file';
}