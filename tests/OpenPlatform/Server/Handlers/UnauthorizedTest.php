<?php


namespace Sinta\Wechat\Tests\OpenPlatform;

use Sinta\Wechat\OpenPlatform\Server\Handlers\Unauthorized;
use Sinta\Wechat\Tests\TestCase;

class UnauthorizedTest extends TestCase
{
    public function testHandle()
    {
        $handler = new Unauthorized();

        $this->assertNull($handler->handle());
    }
}
