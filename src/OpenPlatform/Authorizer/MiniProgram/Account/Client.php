<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Account;


use Sinta\Wechat\OpenPlatform\Authorizer\Aggregate\Account\Client as BaseClient;

class Client extends BaseClient
{
    /**
     * 获取账号基本信息.
     */
    public function getBasicInfo()
    {
        return $this->httpPostJson('cgi-bin/account/getaccountbasicinfo');
    }

    /**
     * 修改头像.
     *
     * @param string $mediaId
     * @param float $left
     * @param float $top
     * @param float $right
     * @param float $bottom
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function updateAvatar(
        string $mediaId,
        float $left = 0,
        float $top = 0,
        float $right = 1,
        float $bottom = 1
    ) {
        $params = [
            'head_img_media_id' => $mediaId,
            'x1' => $left, 'y1' => $top, 'x2' => $right, 'y2' => $bottom,
        ];
        return $this->httpPostJson('cgi-bin/account/modifyheadimage', $params);
    }

    /**
     * 修改功能介绍.
     *
     * @param string $signature 功能介绍（简介）
     */
    public function updateSignature(string $signature)
    {
        $params = ['signature' => $signature];
        return $this->httpPostJson('cgi-bin/account/modifysignature', $params);
    }
}