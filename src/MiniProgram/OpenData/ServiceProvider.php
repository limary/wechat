<?php
namespace Sinta\Wechat\MiniProgram\OpenData;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 小程序数据分析服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\MiniProgram\DataCube
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['open_data'] = function ($app){
            return new Client($app);
        };
    }
}