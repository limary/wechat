<?php
namespace Sinta\Wechat\BasicService\Qrcode;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Qrcode服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\BaseService\Qrcode
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        isset($app['qrcode']) || $app['qrcode'] = function ($app){
            return new Client($app);
        };
    }
}