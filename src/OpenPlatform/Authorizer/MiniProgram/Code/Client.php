<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Code;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 代码管理
 *
 * Class Client
 * @package Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Code
 */
class Client extends BaseClient
{

    /**
     * 为授权的小程序帐号上传小程序代码
     *
     * @param int $templateId
     * @param string $extJson
     * @param string $version
     * @param string $description
     * @return mixed
     */
    public function commit(int $templateId, string $extJson, string $version, string $description)
    {
        return $this->httpPostJson('wxa/commit', [
            'template_id' => $templateId,
            'ext_json' => $extJson,
            'user_version' => $version,
            'user_desc' => $description,
        ]);
    }

    /**
     * 获取体验小程序的体验二维码
     *
     * @return static
     */
    public function getQrCode(string $path = null)
    {
        return $this->requestRaw('wxa/get_qrcode', 'GET', [
            'query' => ['path' => $path],
        ]);
    }


    /**
     * 获取授权小程序帐号的可选类目
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getCategory()
    {
        return $this->httpGet('wxa/get_category');
    }

    /**
     * 获取小程序的第三方提交代码的页面配置
     * 仅供第三方开发者代小程序调用）
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getPage()
    {
        return $this->httpGet('wxa/get_page');
    }

    /**
     * 将第三方提交的代码包提交审核（仅供第三方开发者代小程序调用）
     *
     * @param array $itemList
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function submitAudit(array $itemList)
    {
        return $this->httpPostJson('wxa/submit_audit', [
            'item_list' => $itemList,
        ]);
    }

    /**
     * 查询某个指定版本的审核状态（仅供第三方代小程序调用）
     *
     * @param int $auditId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getAuditStatus(int $auditId)
    {
        return $this->httpPostJson('wxa/get_auditstatus', [
            'auditid' => $auditId,
        ]);
    }

    /**
     * 查询最新一次提交的审核状态（仅供第三方代小程序调用）
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getLatestAuditStatus()
    {
        return $this->httpGet('wxa/get_latest_auditstatus');
    }


    /**
     * 发布已通过审核的小程序（仅供第三方代小程序调用）
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function release()
    {
        return $this->httpPostJson('wxa/release');
    }

    public function withdrawAudit()
    {
        return $this->httpGet('wxa/undocodeaudit');
    }

    public function rollbackRelease()
    {
        return $this->httpGet('wxa/revertcoderelease');
    }


    /**
     * 修改小程序线上代码的可见状态（仅供第三方代小程序调用）
     *
     * @param string  $action  (close|open)
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function changeVisitStatus(string $action)
    {
        return $this->httpPostJson('wxa/change_visitstatus', [
            'action' => $action,
        ]);
    }

    /**
     * 分阶段发布
     *
     * @param int $grayPercentage
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function grayRelease(int $grayPercentage)
    {
        return $this->httpPostJson('wxa/grayrelease', [
            'gray_percentage' => $grayPercentage,
        ]);
    }

    /**
     * 取消分阶段发布
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function revertGrayRelease()
    {
        return $this->httpGet('wxa/revertgrayrelease');
    }

    /**
     * 查询当前分阶段发布详情
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getGrayRelease()
    {
        return $this->httpGet('wxa/getgrayreleaseplan');
    }

    /**
     * 查询当前设置的最低基础库版本及各版本用户占比.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getSupportVersion()
    {
        return $this->httpPostJson('cgi-bin/wxopen/getweappsupportversion');
    }

    /**
     * 设置最低基础库版本.
     *
     * @param string $version
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function setSupportVersion(string $version)
    {
        return $this->httpPostJson('cgi-bin/wxopen/setweappsupportversion', [
            'version' => $version,
        ]);
    }
}