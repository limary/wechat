<?php
namespace Sinta\Wechat\Kernel\Messages;

/**
 * 视频消息
 *
 * Class Video
 * @package Sinta\Wechat\Kernel\Messages
 */
class Video extends Media
{
    protected $type = 'video';

    protected $properties = [
        'title',
        'description',
        'media_id',
        'thumb_media_id',
    ];

    public function __construct(string $mediaId, array $attributes = [])
    {
        parent::__construct($mediaId, 'video', $attributes);
    }


    public function toXmlArray()
    {
        return [
            'Video' => [
                'MediaId' => $this->get('media_id'),
                'Title' => $this->get('title'),
                'Description' => $this->get('description'),
            ],
        ];
    }

}