<?php
namespace Sinta\Wechat\OfficialAccount\ShakeAround;


use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;

class ShakeAround extends Client
{
    public function __get($property)
    {
        if (isset($this->app["shake_around.{$property}"])) {
            return $this->app["shake_around.{$property}"];
        }

        throw new InvalidArgumentException(sprintf('No shake_around service named "%s".', $property));
    }
}