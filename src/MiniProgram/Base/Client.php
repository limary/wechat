<?php
namespace Sinta\Wechat\MiniProgram\Base;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    public function getPaidUnionid($openid, $options = [])
    {
        return $this->httpGet('wxa/getpaidunionid', compact('openid') + $options);
    }
}