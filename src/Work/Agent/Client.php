<?php
namespace Sinta\Wechat\Work\Agent;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{

    public function get(int $agentId)
    {
        $params = [
            'agentid' => $agentId
        ];

        return $this->httpGet('cgi-bin/agent/get',$params);
    }

    public function set(int $agentId, array $attributes)
    {
        return $this->httpPostJson('cgi-bin/agent/set', array_merge(['agentid' => $agentId], $attributes));
    }


    public function list()
    {
        return $this->httpGet('cgi-bin/agent/list');
    }
}