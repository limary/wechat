<?php
namespace Sinta\Wechat\OfficialAccount\Server;

use Sinta\Wechat\Kernel\ServerGuard;


class Guard extends ServerGuard
{
    protected function shouldReturnRawResponse(): bool
    {
        return !is_null($this->app['request']->get('echostr'));
    }
}