<?php

namespace Sinta\Wechat\Payment\Reverse;

use Sinta\Wechat\Payment\Kernel\BaseClient;

/**
 * 撤销订单服务方法
 *
 * Class Client
 * @package Sinta\Wechat\Payment\Reverse
 */
class Client extends BaseClient
{
    /**
     * 通订单号撤销订单
     *
     * @param string $outTradeNumber
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|Client
     */
    public function byOutTradeNumber(string $outTradeNumber)
    {
        return $this->reverse($outTradeNumber, 'out_trade_no');
    }

    /**
     * 通过交易号撤销
     *
     * @param string $transactionId
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|Client
     */
    public function byTransactionId(string $transactionId)
    {
        return $this->reverse($transactionId, 'transaction_id');
    }

    /**
     * 撤销订单
     *
     * @param string $number
     * @param string $type
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    protected function reverse(string $number, string $type)
    {
        $params = [
            'appid' => $this->app['config']->app_id,
            $type => $number,
        ];

        return $this->safeRequest('secapi/pay/reverse', $params);
    }
}