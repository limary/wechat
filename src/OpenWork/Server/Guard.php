<?php
namespace Sinta\Wechat\OpenWork\Server;

use Sinta\Wechat\Kernel\ServerGuard;
use Sinta\Wechat\Kernel\Encryptor;

class Guard extends ServerGuard
{
    protected $alwaysValidate = true;

    public function validate()
    {
        return $this;
    }

    protected function shouldReturnRawResponse(): bool
    {
        return !is_null($this->app['request']->get('echostr'));
    }
    protected function isSafeMode(): bool
    {
        return true;
    }

    /**
     * @param array $message
     * @return string
     * @throws \Sinta\Wechat\Kernel\Exceptions\RuntimeException
     */
    protected function decryptMessage(array $message)
    {
        $encryptor = new Encryptor($message['ToUserName'], $this->app['config']->get('token'), $this->app['config']->get('aes_key'));
        return $message = $encryptor->decrypt(
            $message['Encrypt'],
            $this->app['request']->get('msg_signature'),
            $this->app['request']->get('nonce'),
            $this->app['request']->get('timestamp')
        );
    }
}