<?php
namespace Sinta\Wechat\OfficialAccount\POI;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 公共号门店服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount\POI
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['poi'] = function ($app) {
            return new Client($app);
        };
    }
}