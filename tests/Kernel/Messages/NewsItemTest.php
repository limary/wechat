<?php


namespace Sinta\Wechat\Tests\Kernel\Messages;

use Sinta\Wechat\Kernel\Messages\NewsItem;
use Sinta\Wechat\Tests\TestCase;

class NewsItemTest extends TestCase
{
    public function testToXmlArray()
    {
        $message = new NewsItem([
                'title' => '发布了',
                'description' => ' 于今天发布了',
                'url' => 'http://eas0-released.html',
                'image' => 'http://img0om/4.0.jpg',
            ]);

        $this->assertSame([
            'Title' => '发布了',
            'Description' => ' 于今天发布了',
            'Url' => 'http://eas0-released.html',
            'PicUrl' => 'http://img0om/4.0.jpg',
        ], $message->toXmlArray());
    }
}
