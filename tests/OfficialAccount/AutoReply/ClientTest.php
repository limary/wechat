<?php
namespace Sinta\Wechat\Tests\OfficialAccount\AutoReply;

use Sinta\Wechat\OfficialAccount\AutoReply\Client;
use Sinta\Wechat\Tests\TestCase;

class ClientTest extends TestCase
{
    public function testCurrent()
    {
        $client = $this->mockApiClient(Client::class);
        $client->expects()->httpGet('cgi-bin/get_current_autoreply_info')->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->current());
    }
}
