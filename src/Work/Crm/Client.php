<?php
namespace Sinta\Wechat\Work\Crm;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    public function getExternalContact($externalUserId)
    {
        return $this->httpGet('cgi-bin/crm/get_external_contact', [
            'external_userid' => $externalUserId,
        ]);
    }
}