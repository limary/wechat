<?php

namespace Sinta\Wechat\Payment\Refund;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 退款服务
 *
 * Class ServiceProvider
 * @see https://pay.weixin.qq.com/wiki/doc/api/micropay_sl_jw.php?chapter=9_5
 * @package Sinta\Wechat\Payment\Refund
 */
class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['refund'] = function ($app) {
            return new Client($app);
        };
    }
}