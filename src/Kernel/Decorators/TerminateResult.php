<?php
namespace Sinta\Wechat\Kernel\Decorators;


class TerminateResult
{
    public $content;

    public function __construct($content)
    {
        $this->content = $content;
    }
}