<?php
namespace Sinta\Wechat\Tests\OpenPlatform;

use Sinta\Wechat\OpenPlatform\Server\Handlers\Authorized;
use Sinta\Wechat\Tests\TestCase;

class AuthorizedTest extends TestCase
{
    public function testHandle()
    {
        $handler = new Authorized();

        $this->assertNull($handler->handle());
    }
}
