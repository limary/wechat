<?php


namespace Sinta\Wechat\Test\Kernel\Messages;

use Sinta\Wechat\Kernel\Messages\Text;
use Sinta\Wechat\Tests\TestCase;

class TextTest extends TestCase
{
    public function testBasicFeatures()
    {
        $text = new Text('mock-content');

        $this->assertSame('mock-content', $text->content);
    }
}
