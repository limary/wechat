<?php
namespace Sinta\Wechat\Kernel\Messages;

use Mockery\Exception\BadMethodCallException;
use Sinta\Wechat\Kernel\Contracts\MessageInterface;
use Sinta\Wechat\Kernel\Support\XML;
use Sinta\Wechat\Kernel\Traits\HasAttributes;

/**
 * 所有消息的基类
 *
 * Class Message
 * @package Sinta\Wechat\Kernel\Messages
 */
abstract class Message implements MessageInterface
{
    use HasAttributes;

    const TEXT = 2;
    const IMAGE = 4;
    const VOICE = 8;
    const VIDEO = 16;
    const SHORT_VIDEO = 32;
    const LOCATION = 64;
    const LINK = 128;
    const DEVICE_EVENT = 256;
    const DEVICE_TEXT = 512;
    const FILE = 1024;
    const TEXT_CARD = 2048;
    const TRANSFER = 4096;
    const EVENT = 1048576;
    const MINIPROGRAM_PAGE = 2097152;
    const ALL = self::TEXT | self::IMAGE | self::VOICE | self::VIDEO | self::SHORT_VIDEO | self::LOCATION | self::LINK
    | self::DEVICE_EVENT | self::DEVICE_TEXT | self::FILE | self::TEXT_CARD | self::TRANSFER | self::EVENT | self::MINIPROGRAM_PAGE;

    /**
     * 消息的类型
     *
     * @var string
     */
    protected $type;


    protected $id;

    protected $to;

    protected $form;

    protected $properties = [];

    protected $jsonAliases = [];


    public function __construct(array $attributes = [])
    {
        $this->setAttributes($attributes);
    }


    /**
     * 获取消息的类型
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }


    /**
     * 设置消息类型
     *
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * 获取属性
     *
     * @param $property
     * @return mixed
     */
    public function __get($property)
    {
        if(property_exists($this,$property)){
            return $this->$property;
        }
        return $this->getAttribute($property);
    }


    /**
     * 设置属性
     *
     * @param $property
     * @param $value
     * @return $this
     */
    public function __set($property,$value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        } else {
            $this->setAttribute($property, $value);
        }
        return $this;
    }


    public function transformForJsonRequestWithoutType(array $appends = [])
    {
        return $this->transformForJsonRequest($appends, false);
    }


    public function transformForJsonRequest(array $appends = [], $withType = true): array
    {
        if (!$withType) {
            return $this->propertiesToArray([], $this->jsonAliases);
        }
        $messageType = $this->getType();
        $data = array_merge(['msgtype' => $messageType], $appends);

        $data[$messageType] = array_merge($data[$messageType] ?? [], $this->propertiesToArray([], $this->jsonAliases));

        return $data;
    }


    public function transformToXml(array $appends = [], bool $returnAsArray = false): string
    {
        $data = array_merge(['MsgType' => $this->getType()], $this->toXmlArray(), $appends);

        return $returnAsArray ? $data : XML::build($data);
    }


    protected function propertiesToArray(array $data, array $aliases = []): array
    {
        $this->checkRequiredAttributes();

        foreach ($this->attributes as $property => $value) {
            if (is_null($value) && !$this->isRequired($property)) {
                continue;
            }
            $alias = array_search($property, $aliases, true);

            $data[$alias ?: $property] = $this->get($property);
        }

        return $data;
    }


    public function toXmlArray()
    {
        throw new BadMethodCallException(sprintf('Class "%s" cannot support transform to XML message.', __CLASS__));
    }
}