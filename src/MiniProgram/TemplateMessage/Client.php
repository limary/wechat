<?php
namespace Sinta\Wechat\MiniProgram\TemplateMessage;

use Sinta\Wechat\OfficialAccount\TemplateMessage\Client as BaseClient;

/**
 * 模板消息
 *
 * Class Client
 * @package Sinta\Wechat\MiniProgram\TemplateMessage
 *
 * @https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1500465446_j4CgR&token=&lang=zh_CN
 */
class Client extends BaseClient
{
    const API_SEND = 'cgi-bin/message/wxopen/template/send';


    protected $message = [
        'touser' => '',
        'template_id' => '',
        'page' => '',
        'form_id' => '',
        'data' => [],
        'emphasis_keyword' => '',
    ];


    protected $required = ['touser', 'template_id', 'form_id'];


    /**
     * 获取小程序模板库标题列表
     *
     * @param int $offset  offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。
     * @param int $count   offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list(int $offset, int $count)
    {
        return $this->httpPostJson('cgi-bin/wxopen/template/library/list',compact('offset','count'));
    }

    /**
     * 获取模板库某个模板标题下关键词库
     *
     * @param string $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get(string $id)
    {
        return $this->httpPostJson('cgi-bin/wxopen/template/library/get', compact('id'));
    }

    /**
     * 组合模板并添加至帐号下的个人模板库
     *
     * @param string $id
     * @param array $keyword
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function add(string $id, array $keyword)
    {
        return $this->httpPostJson('cgi-bin/wxopen/template/add', [
            'id' => $id,
            'keyword_id_list' => $keyword,
        ]);
    }

    public function delete(string $templateId)
    {
        return $this->httpPostJson('cgi-bin/wxopen/template/del', [
            'template_id' => $templateId,
        ]);
    }

    /**
     * 为便于第三方开发者对帐号下已存在的模板进行操作，现提供如下接口：
     *
     * @param int $offset
     * @param int $count
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getTemplates(int $offset, int $count)
    {
        return $this->httpPostJson('cgi-bin/wxopen/template/list', compact('offset', 'count'));
    }
}