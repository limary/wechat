<?php
namespace Sinta\Wechat\OfficialAccount\ShakeAround;

use Sinta\Wechat\Kernel\Client as BaseClient;

class GroupClient extends BaseClient
{
    public function create(string $name)
    {
        $params = [
            'group_name' => $name,
        ];

        return $this->httpPostJson('shakearound/device/group/add', $params);
    }


    public function update(int $groupId, string $name)
    {
        $params = [
            'group_id' => $groupId,
            'group_name' => $name,
        ];

        return $this->httpPostJson('shakearound/device/group/update', $params);
    }


    public function delete(int $groupId)
    {
        $params = [
            'group_id' => $groupId,
        ];

        return $this->httpPostJson('shakearound/device/group/delete', $params);
    }

    public function list(int $begin, int $count)
    {
        $params = [
            'begin' => $begin,
            'count' => $count,
        ];

        return $this->httpPostJson('shakearound/device/group/getlist', $params);
    }

    public function get(int $groupId, int $begin, int $count)
    {
        $params = [
            'group_id' => $groupId,
            'begin' => $begin,
            'count' => $count,
        ];

        return $this->httpPostJson('shakearound/device/group/getdetail', $params);
    }

    public function addDevices(int $groupId, array $deviceIdentifiers)
    {
        $params = [
            'group_id' => $groupId,
            'device_identifiers' => $deviceIdentifiers,
        ];

        return $this->httpPostJson('shakearound/device/group/adddevice', $params);
    }

    public function removeDevices(int $groupId, array $deviceIdentifiers)
    {
        $params = [
            'group_id' => $groupId,
            'device_identifiers' => $deviceIdentifiers,
        ];

        return $this->httpPostJson('shakearound/device/group/deletedevice', $params);
    }

}