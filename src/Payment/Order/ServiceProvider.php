<?php

namespace Sinta\Wechat\Payment\Order;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 *
 * 订单服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\Payment\Order
 */
class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['order'] = function ($app){
            return new Client($app);
        };
    }
}