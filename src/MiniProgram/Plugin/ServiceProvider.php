<?php
namespace Sinta\Wechat\MiniProgram\Plugin;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['plugin'] = function ($app) {
            return new Client($app);
        };
    }
}