<?php
namespace Sinta\Wechat\OpenPlatform\Server;

use Sinta\Wechat\Kernel\ServerGuard;
use Sinta\Wechat\OpenPlatform\Server\Handlers\Authorized;
use Sinta\Wechat\OpenPlatform\Server\Handlers\Unauthorized;
use Sinta\Wechat\OpenPlatform\Server\Handlers\UpdateAuthorized;
use Sinta\Wechat\OpenPlatform\Server\Handlers\VerifyTicketRefreshed;
use Symfony\Component\HttpFoundation\Response;



class Guard extends ServerGuard
{
    const EVENT_AUTHORIZED = 'authorized';
    const EVENT_UNAUTHORIZED = 'unauthorized';
    const EVENT_UPDATE_AUTHORIZED = 'updateauthorized';
    const EVENT_COMPONENT_VERIFY_TICKET = 'component_verify_ticket';


    /**
     * 处理接收
     *
     * @return Response
     */
    protected function resolve():Response
    {
        $this->registerHandlers();

        $message = $this->getMessage();
        if(isset($message['InfoType'])){
            $this->dispatch($message['InfoType'],$message);
        }

        return new Response(static::SUCCESS_EMPTY_RESPONSE);
    }

    /**
     * 注册处理事件
     */
    protected function registerHandlers()
    {
        $this->on(self::EVENT_AUTHORIZED, Authorized::class);
        $this->on(self::EVENT_UNAUTHORIZED, Unauthorized::class);
        $this->on(self::EVENT_UPDATE_AUTHORIZED, UpdateAuthorized::class);
        $this->on(self::EVENT_COMPONENT_VERIFY_TICKET, VerifyTicketRefreshed::class);
    }
}