<?php



namespace Sinta\Wechat\Tests\OfficialAccount\Base;

use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\OfficialAccount\Base\Client;
use Sinta\Wechat\Tests\TestCase;

class ClientTest extends TestCase
{
    public function testClearQuota()
    {
        $client = $this->mockApiClient(Client::class, [], new ServiceContainer(['app_id' => '123456']));

        $client->expects()->httpPostJson('cgi-bin/clear_quota', [
            'appid' => '123456',
        ])->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->clearQuota());
    }

    public function testGetValidIps()
    {
        $client = $this->mockApiClient(Client::class);

        $client->expects()->httpGet('cgi-bin/getcallbackip')->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->getValidIps());
    }
}
