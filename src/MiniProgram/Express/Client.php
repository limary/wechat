<?php
namespace Sinta\Wechat\MiniProgram\Express;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    /**
     * 获取支持的快递公司列表
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/getAllDelivery.html
     */
    public function listProviders()
    {
        return $this->httpGet('cgi-bin/express/business/delivery/getall');
    }

    /**
     * 生成运单
     *
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/addOrder.html
     * @param array $params
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function createWaybill(array $params = [])
    {
        return $this->httpPostJson('cgi-bin/express/business/order/add', $params);
    }

    /**
     * 取消运单
     *
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/cancelOrder.html
     *
     * @param array $params
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deleteWaybill(array $params = [])
    {
        return $this->httpPostJson('cgi-bin/express/business/order/cancel', $params);
    }

    /**
     * 获取运单数据
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/getOrder.html
     *
     * @param array $params
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getWaybill(array $params = [])
    {
        return $this->httpPostJson('cgi-bin/express/business/order/get', $params);
    }

    /**
     * 查询运单轨迹
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/getPath.html
     *
     * @param array $params
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getWaybillTrack(array $params = [])
    {
        return $this->httpPostJson('cgi-bin/express/business/path/get', $params);
    }

    /**
     * 获取电子面单余额
     *
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/getQuota.html
     * @param string $deliveryId
     * @param string $bizId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getBalance(string $deliveryId, string $bizId)
    {
        return $this->httpPostJson('cgi-bin/express/business/quota/get', [
            'delivery_id' => $deliveryId,
            'biz_id' => $bizId,
        ]);
    }

    /**
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/updatePrinter.html
     *
     * @param string $openid
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function bindPrinter(string $openid)
    {
        return $this->httpPostJson('cgi-bin/express/business/printer/update', [
            'update_type' => 'bind',
            'openid' => $openid,
        ]);
    }

    public function unbindPrinter(string $openid)
    {
        return $this->httpPostJson('cgi-bin/express/business/printer/update', [
            'update_type' => 'unbind',
            'openid' => $openid,
        ]);
    }

    /**
     * 获取打印员
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getPrinters()
    {
        return $this->httpGet('cgi-bin/express/business/printer/getall');
    }

}