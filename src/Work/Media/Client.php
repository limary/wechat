<?php
namespace Sinta\Wechat\Work\Media;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Http\StreamResponse;

/**
 * 所有文件size必须大于5个字节

图片（image）:2MB，支持JPG,PNG格式

语音（voice）：2MB，播放长度不超过60s，支持AMR格式

视频（video）：10MB，支持MP4格式

普通文件（file）：20MB
 *
 * Class Client
 * @package Sinta\Wechat\Work\Media
 */
class Client extends BaseClient
{
    /**
     * 获取素材
     *
     * @param string $mediaId 媒体文件id。最大长度为256字节
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get(string $mediaId)
    {
        $response = $this->requestRaw('cgi-bin/media/get','GET',[
            'query' => [
                'media_id' => $mediaId
            ],
        ]);

        if(false !== stripos($response->getHeaderLine('Content-Type'),'text/plain')){
            return $this->castResponseToType($response, $this->app['config']->get('response_type'));
        }
        return StreamResponse::buildFromPsrResponse($response);
    }

    /**
     *
     *
     * @param string $path
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function uploadImage(string $path)
    {
        return $this->upload('image', $path);
    }

    public function uploadVoice(string $path)
    {
        return $this->upload('voice', $path);
    }

    public function uploadVideo(string $path)
    {
        return $this->upload('video', $path);
    }

    public function uploadFile(string $path)
    {
        return $this->upload('file', $path);
    }

    public function upload(string $type, string $path)
    {
        $files = [
            'media' => $path,
        ];
        return $this->httpUpload('cgi-bin/media/upload', $files, [], compact('type'));
    }

}