<?php

namespace Sinta\Wechat\Payment\Notify;

use Closure;
use Sinta\Wechat\Kernel\Support;
use Sinta\Wechat\Kernel\Support\XML;
use Sinta\Wechat\Kernel\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;
use Sinta\WeChat\Payment\Kernel\Exceptions\InvalidSignException;

/**
 * 通知处理
 *
 * Class Handler
 * @package Sinta\Wechat\Payment\Notify
 */
abstract class Handler
{
    const SUCCESS = 'SUCCESS';
    const FAIL = 'FAIL';

    protected $app;

    /**
     * @var array
     */
    protected $message;


    protected $fail;


    protected $attributes = [];


    protected $check = true;


    protected $sign = false;


    public function __construct($app)
    {
        $this->app = $app;
    }


    abstract public function handle(Closure $closure);

    /**
     * 失败消息
     *
     * @param string $message
     */
    public function fail(string $message)
    {
        $this->fail = $message;
    }


    /**
     * @param array $attributes
     * @param bool $sign
     * @return $this
     */
    public function respondWith(array $attributes, bool $sign = false)
    {
        $this->attributes = $attributes;
        $this->sign = $sign;

        return $this;
    }


    public function toResponse(): Response
    {
        $base = [
            'return_code' => is_null($this->fail) ? static::SUCCESS : static::FAIL,
            'return_msg' => $this->fail,
        ];

        $attributes = array_merge($base, $this->attributes);

        if ($this->sign) {
            $attributes['sign'] = Support\generate_sign($attributes, $this->app['config']->key);
        }

        return new Response(XML::build($attributes));
    }

    /**
     * 消息获取
     *
     * @return array
     * @throws Exception
     * @throws InvalidSignException
     */
    public function getMessage(): array
    {
        if (!empty($this->message)) {
            return $this->message;
        }

        try {
            $message = XML::parse(strval($this->app['request']->getContent()));
        } catch (\Throwable $e) {
            throw new Exception('Invalid request XML: '.$e->getMessage(), 400);
        }

        if (!is_array($message) || empty($message)) {
            throw new Exception('Invalid request XML.', 400);
        }

        if ($this->check) {
            $this->validate($message);
        }

        return $this->message = $message;
    }

    /**
     * 解密消息
     *
     * @param string $key
     * @return null|string
     * @throws Exception
     * @throws InvalidSignException
     */
    public function decryptMessage(string $key)
    {
        $message = $this->getMessage();
        if (empty($message[$key])) {
            return null;
        }

        return Support\AES::decrypt(
            base64_decode($message[$key], true), md5($this->app['config']->key), '', OPENSSL_RAW_DATA, 'AES-256-ECB'
        );
    }


    /**
     * @param array $message
     * @throws InvalidSignException
     */
    protected function validate(array $message)
    {
        $sign = $message['sign'];
        unset($message['sign']);

        if (Support\generate_sign($message, $this->app['config']->key) !== $sign) {
            throw new InvalidSignException();
        }
    }

    protected function strict($result)
    {
        if ($result !== true && is_null($this->fail)) {
            $this->fail(strval($result));
        }
    }

}