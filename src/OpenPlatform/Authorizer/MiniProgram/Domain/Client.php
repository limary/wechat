<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Domain;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 授权小程序域名修改
 *
 * 授权给第三方的小程序，其服务器域名只可以为第三方的服务器，
 * 当小程序通过第三方发布代码上线后，小程序原先自己配置的服务器域名将被删除，
 * 只保留第三方平台的域名，所以第三方平台在代替小程序发布代码之前，需要调用接口为小程序添加第三方自身的域名。
 *
 * Class Client
 * @package Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Domain
 */
class Client extends BaseClient
{
    /**
     *
     *
     * @param array $params
     *
     * `action `  add添加, delete删除, set覆盖, get获取。当参数是get时不需要填四个域名字段。
     * `requestdomain`	 request合法域名，当action参数是get时不需要此字段
     * `wsrequestdomain` 'socket合法域名，当action参数是get时不需要此字段'
     * 'uploaddomain'     'uploadFile合法域名，当action参数是get时不需要此字段'
     * 'downloaddomain'   downloadFile合法域名，当action参数是get时不需要此字段
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function modify(array $params)
    {
        return $this->httpPostJson('wxa/modify_domain', $params);
    }

    /**
     *  设置小程序业务域名.
     *
     * @param array $domains
     * @param string $action
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function setWebviewDomain(array $domains, $action = 'add')
    {
        return $this->httpPostJson('wxa/setwebviewdomain', [
            'action' => $action,
            'webviewdomain' => $domains,
        ]);
    }
}