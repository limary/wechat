<?php
namespace Sinta\Wechat\Work\Base;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    public function getCallbackIp()
    {
        return $this->httpGet('cgi-bin/getcallbackip');
    }
}