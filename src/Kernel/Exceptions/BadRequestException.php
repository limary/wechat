<?php
namespace Sinta\Wechat\Kernel\Exceptions;

/**
 * Class BadRequestException
 * @package Sinta\Wechat\Kernel\Exceptions
 */
class BadRequestException extends Exception
{

}