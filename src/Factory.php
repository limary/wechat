<?php
namespace Sinta\Wechat;

/**
 * 工厂方法
 *
 * @method static \Sinta\Wechat\Payment\Application            payment(array $config)
 * @method static \Sinta\Wechat\MiniProgram\Application        miniProgram(array $config)
 * @method static \Sinta\Wechat\OpenPlatform\Application       openPlatform(array $config)
 * @method static \Sinta\Wechat\OfficialAccount\Application    officialAccount(array $config)
 * @method static \Sinta\Wechat\BasicService\Application       basicService(array $config)
 * @method static \Sinta\Wechat\Work\Application               work(array $config)
 * @method static \Sinta\Wechat\OpenWork\Application           openWork(array $config)
 *
 * Class Factory
 * @package Sinta\Wechat
 */
class Factory
{

    /**
     * 创建应用
     *
     * @param $name
     * @param array $config
     * @return mixed
     */
    public static function make($name,array $config)
    {
        $namespace = Kernel\Support\Str::studly($name);
        $application = "\\Sinta\\Wechat\\{$namespace}\\Application";

        return new $application($config);
    }

    /**
     * 静态调用
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}