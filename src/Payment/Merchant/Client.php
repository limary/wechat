<?php

namespace Sinta\Wechat\Payment\Merchant;


use Sinta\Wechat\Payment\Kernel\BaseClient;

class Client extends BaseClient
{

    public function addSubMerchant(array $params)
    {
        return $this->manage($params, ['action' => 'add']);
    }


    public function querySubMerchantByMerchantId(string $id)
    {
        $params = [
            'micro_mch_id' => $id,
        ];
        return $this->manage($params, ['action' => 'query']);
    }

    public function querySubMerchantByWeChatId(string $id)
    {
        $params = [
            'recipient_wechatid' => $id,
        ];
        return $this->manage($params, ['action' => 'query']);
    }


    protected function manage(array $params, array $query)
    {
        $params = array_merge($params, [
            'appid' => $this->app['config']->app_id,
            'nonce_str' => '',
            'sub_mch_id' => '',
            'sub_appid' => '',
        ]);
        return $this->safeRequest('secapi/mch/submchmanage', $params, 'post', compact('query'));
    }
}