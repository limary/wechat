<?php
namespace Sinta\Wechat\MiniProgram\Auth;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 认证服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\MiniProgram\Auth
 */
class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        !isset($app['access_token']) && $app['access_token'] = function ($app) {
            return new AccessToken($app);
        };

        !isset($app['auth']) && $app['auth'] = function ($app) {
            return new Client($app);
        };
    }
}