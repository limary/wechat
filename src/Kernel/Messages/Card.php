<?php
namespace Sinta\Wechat\Kernel\Messages;

class Card extends Message
{
    protected $type = 'wxcard';

    protected $properties = ['card_id'];

    /**
     * Media constructor.
     *
     * @param string $cardId
     */
    public function __construct(string $cardId)
    {
        parent::__construct(['card_id' => $cardId]);
    }
}