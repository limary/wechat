<?php
namespace Sinta\Wechat\Work\Crm;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['crm'] = function ($app) {
            return new Client($app);
        };
    }
}