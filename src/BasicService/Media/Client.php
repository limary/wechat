<?php
namespace Sinta\Wechat\BasicService\Media;

use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;
use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Http\StreamResponse;


/**
 * 素材管理
 *
 * Class Client
 * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1444738726
 * @package Sinta\Wechat\BasicService\Media
 */
class Client extends BaseClient
{
    protected $baseUri = 'https://api.weixin.qq.com/cgi-bin/';

    protected $allowTypes =['image', 'voice', 'video', 'thumb'];


    /**
     * 上传图片
     * 2M，支持PNG\JPEG\JPG\GIF格式
     * @param $path
     * @return mixed
     */
    public function uploadImage($path)
    {
        return $this->upload('image',$path);
    }

    /**
     * 上传视频（10MB，支持MP4格式）
     *
     * @param $path
     * @return mixed
     */
    public function uploadVideo($path)
    {
        return $this->upload('video', $path);
    }

    /**
     * 上传语言（2M，播放长度不超过60s，支持AMR\MP3格式）
     *
     * @param $path
     * @return mixed
     */
    public function uploadVoice($path)
    {
        return $this->upload('voice', $path);
    }

    /**
     * 缩略图(64KB，支持JPG格式)
     *
     * @param $path
     * @return mixed
     */
    public function uploadThumb($path)
    {
        return $this->upload('thumb', $path);
    }

    /**
     * 上传素材
     *
     * @param string $type 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     * @param $path
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function upload($type, $path)
    {
        if (!file_exists($path) || !is_readable($path)) {
            throw new InvalidArgumentException(sprintf("File does not exist, or the file is unreadable: '%s'", $path));
        }

        if (!in_array($type, $this->allowTypes, true)) {
            throw new InvalidArgumentException(sprintf("Unsupported media type: '%s'", $type));
        }

        return $this->httpUpload('media/upload', ['media' => $path], ['type' => $type]);
    }


    public function uploadVideoForBroadcasting(string $path, string $title, string $description)
    {
        $response = $this->uploadVideo($path);
        $arrayResponse = $this->transformResponseToType($response, 'array');

        if (!empty($arrayResponse['media_id'])) {
            return $this->createVideoForBroadcasting($arrayResponse['media_id'], $title, $description);
        }

        return $response;
    }


    public function createVideoForBroadcasting(string $mediaId, string $title, string $description)
    {
        return $this->httpPostJson('media/uploadvideo', [
            'media_id' => $mediaId,
            'title' => $title,
            'description' => $description,
        ]);
    }


    /**
     * 获取临时素材
     *
     * @param $mediaId
     * @return static
     */
    public function get($mediaId)
    {
        $response = $this->requestRaw('media/get', 'GET', [
            'query' => [
                'media_id' => $mediaId,
            ],
        ]);

        if (false !== stripos($response->getHeaderLine('Content-disposition'), 'attachment')) {
            return StreamResponse::buildFromPsrResponse($response);
        }

        return $this->castResponseToType($response, $this->app['config']->get('response_type'));
    }


    public function getJssdkMedia(string $mediaId)
    {
        $response = $this->requestRaw('media/get/jssdk', 'GET', [
            'query' => [
                'media_id' => $mediaId,
            ],
        ]);
        if (false !== stripos($response->getHeaderLine('Content-disposition'), 'attachment')) {
            return StreamResponse::buildFromPsrResponse($response);
        }
        return $this->castResponseToType($response, $this->app['config']->get('response_type'));
    }


}