<?php
namespace Sinta\Wechat\OfficialAccount\Comment;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['comment'] = function ($app) {
            return new Client($app);
        };
    }
}