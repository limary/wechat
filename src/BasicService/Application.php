<?php
namespace Sinta\Wechat\BasicService;

use Sinta\Wechat\Kernel\ServiceContainer;

/**
 * 基础服务
 *
 * Class Application
 * @package Sinta\Wechat\BasicService
 */
class Application extends ServiceContainer
{
    protected $providers = [
        Jssdk\ServiceProvider::class,
        Qrcode\ServiceProvider::class,
        Media\ServiceProvider::class,
        Url\ServiceProvider::class,
        ContentSecurity\ServiceProvider::class,
    ];
}