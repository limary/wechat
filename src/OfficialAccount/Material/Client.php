<?php
namespace Sinta\Wechat\OfficialAccount\Material;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;
use Sinta\Wechat\Kernel\Http\StreamResponse;
use Sinta\Wechat\Kernel\Messages\Article;

/**
 * 永久素材
 *
 * Class Client
 * @package Sinta\Wechat\OfficialAccount\Material
 */
class Client extends BaseClient
{
    protected $allowTypes = ['image','voice','video','thumb','news_image'];

    /**
     * 更新图片
     *
     * @param string $path
     * @return \Psr\Http\Message\ResponseInterface
     * @throws InvalidArgumentException
     */
    public function uploadImage(string $path)
    {
        return $this->upload('image', $path);
    }


    /**
     * @param string $path
     * @return \Psr\Http\Message\ResponseInterface
     * @throws InvalidArgumentException
     */
    public function uploadVoice(string $path)
    {
        return $this->upload('voice', $path);
    }

    /**
     * @param string $path
     * @return \Psr\Http\Message\ResponseInterface
     * @throws InvalidArgumentException
     */
    public function uploadThumb(string $path)
    {
        return $this->upload('thumb', $path);
    }

    /**
     * @param string $path
     * @param string $title
     * @param string $description
     * @return \Psr\Http\Message\ResponseInterface
     * @throws InvalidArgumentException
     */
    public function uploadVideo(string $path, string $title, string $description)
    {
        $params = [
            'description' => json_encode(
                [
                    'title' => $title,
                    'introduction' => $description,
                ], JSON_UNESCAPED_UNICODE),
        ];

        return $this->upload('video', $path, $params);
    }

    public function uploadArticle($articles)
    {
        if ($articles instanceof Article || !empty($articles['title'])) {
            $articles = [$articles];
        }

        $params = ['articles' => array_map(function ($article) {
            if ($article instanceof Article) {
                return $article->transformForJsonRequestWithoutType();
            }

            return $article;
        }, $articles)];

        return $this->httpPostJson('cgi-bin/material/add_news', $params);
    }


    public function updateArticle(string $mediaId, $article, int $index = 0)
    {
        if ($article instanceof Article) {
            $article = $article->transformForJsonRequestWithoutType();
        }

        $params = [
            'media_id' => $mediaId,
            'index' => $index,
            'articles' => isset($article['title']) ? $article : (isset($article[$index]) ? $article[$index] : []),
        ];

        return $this->httpPostJson('cgi-bin/material/update_news', $params);
    }

    public function uploadArticleImage(string $path)
    {
        return $this->upload('news_image', $path);
    }

    public function get(string $mediaId)
    {
        $response = $this->requestRaw('cgi-bin/material/get_material', 'POST', ['json' => ['media_id' => $mediaId]]);

        if (false !== stripos($response->getHeaderLine('Content-disposition'), 'attachment')) {
            return StreamResponse::buildFromPsrResponse($response);
        }

        return $this->castResponseToType($response, $this->app['config']->get('response_type', 'array'));
    }


    public function delete(string $mediaId)
    {
        return $this->httpPostJson('cgi-bin/material/del_material', ['media_id' => $mediaId]);
    }


    public function list(string $type, int $offset = 0, int $count = 20)
    {
        $params = [
            'type' => $type,
            'offset' => intval($offset),
            'count' => min(20, $count),
        ];

        return $this->httpPostJson('cgi-bin/material/batchget_material', $params);
    }

    public function stats()
    {
        return $this->httpGet('cgi-bin/material/get_materialcount');
    }

    /**
     * 上传
     *
     * @param string $type
     * @param string $path
     * @param array $form
     * @return \Psr\Http\Message\ResponseInterface
     * @throws InvalidArgumentException
     */
    public function upload(string $type, string $path, array $form = [])
    {
        if (!file_exists($path) || !is_readable($path)) {
            throw new InvalidArgumentException(sprintf('File does not exist, or the file is unreadable: "%s"', $path));
        }

        $form['type'] = $type;

        return $this->httpUpload($this->getApiByType($type), ['media' => $path], $form);
    }


    public function getApiByType(string $type)
    {
        switch ($type) {
            case 'news_image':
                return 'cgi-bin/media/uploadimg';
            default:
                return 'cgi-bin/material/add_material';
        }
    }

}