<?php
namespace Sinta\Wechat\OpenPlatform\Server\Handlers;


use Sinta\Wechat\Kernel\Contracts\EventHandlerInterface;
use Sinta\Wechat\OpenPlatform\Application;

/**
 * Class VerifyTicketRefreshed
 *
 * @package Sinta\Wechat\OpenPlatform\Server\Handlers
 */
class VerifyTicketRefreshed implements EventHandlerInterface
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function handle($payload = null)
    {
        if (!empty($payload['ComponentVerifyTicket'])) {
            $this->app['verify_ticket']->setTicket($payload['ComponentVerifyTicket']);
        }
    }
}