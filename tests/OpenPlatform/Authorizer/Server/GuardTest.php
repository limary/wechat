<?php

namespace Sinta\Wechat\Tests\OpenPlatform\Authorizer\Server;

use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\OpenPlatform\Authorizer\Server\Guard;
use Sinta\Wechat\Tests\TestCase;

/**
 * Class GuardTest.
 *
 * @author overtrue <i@overtrue.me>
 */
class GuardTest extends TestCase
{
    public function testGetToken()
    {
        $encryptor = \Mockery::mock('stdClass');
        $encryptor->expects()->getToken()->andReturn('token');

        $app = new ServiceContainer([], [
            'encryptor' => $encryptor,
        ]);
        $guard = \Mockery::mock(Guard::class, [$app])->makePartial();

        $this->assertSame('token', $guard->getToken());
    }
}
