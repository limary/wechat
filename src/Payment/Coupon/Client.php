<?php
namespace Sinta\Wechat\Payment\Coupon;

use Sinta\Wechat\Payment\Kernel\BaseClient;

/**
 * 代金券方法
 *
 * Class Client
 * @package Sinta\Wechat\Payment\Coupon
 */
class Client extends BaseClient
{

    /**
     * 发放代金券
     *
     * @param array $params
     * @see https://pay.weixin.qq.com/wiki/doc/api/tools/sp_coupon.php?chapter=12_3&index=4
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function send(array $params)
    {
        $params['appid'] = $this->app['config']->app_id;
        $params['openid_count'] = 1;
        return $this->safeRequest('mmpaymkttransfers/send_coupon',$params);
    }

    /**
     * 查询代金券批次
     *
     * @param array $params
     * @see https://pay.weixin.qq.com/wiki/doc/api/tools/sp_coupon.php?chapter=12_4
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function stock(array $params)
    {
        $params['appid'] = $this->app['config']->app_id;

        return $this->request('mmpaymkttransfers/query_coupon_stock',$params);
    }

    /**
     * 查询代金券信息
     *
     * @param array $params
     * @see https://pay.weixin.qq.com/wiki/doc/api/tools/sp_coupon.php?chapter=12_5&index=6
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function query(array $params)
    {
        $params['appid'] = $this->app['config']->app_id;

        return $this->request('mmpaymkttransfers/querycouponsinfo', $params);
    }

}