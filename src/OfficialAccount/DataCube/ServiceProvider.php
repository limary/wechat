<?php
namespace Sinta\Wechat\OfficialAccount\DataCube;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 用户分析数据服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount\DataCube
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['data_cube'] = function ($app) {
            return new Client($app);
        };
    }
}