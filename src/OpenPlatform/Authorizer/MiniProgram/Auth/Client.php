<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Auth;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\OpenPlatform\Application;


/**
 * 小程序认证
 *
 * Class Client
 * @package Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Auth
 */
class Client extends BaseClient
{
    protected $component;


    public function __construct(ServiceContainer $app, Application $component)
    {
        parent::__construct($app);

        $this->component = $component;
    }

    /**
     * code换session code
     *
     * @param string $code
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function session(string $code)
    {
        $params = [
            'appid' => $this->app['config']['app_id'],
            'js_code' => $code,
            'grant_type' => 'authorization_code',
            'component_appid' => $this->component['config']['app_id'],
            'component_access_token' => $this->component['access_token']->getToken()['component_access_token']
        ];

        return $this->httpGet('sns/component/jscode2session', $params);
    }
}