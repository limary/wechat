<?php
namespace Sinta\Wechat\BasicService\ContentSecurity;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    protected $baseUri = 'https://api.weixin.qq.com/wxa/';


    /**
     * 检查一段文本是否含有违法违规内容
     *
     * @param string $text
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function checkText(string $text)
    {
        $params = [
            'content' => $text,
        ];
        return $this->httpPostJson('msg_sec_check', $params);
    }

    /**
     * 校验一张图片是否含有违法违规内容
     *
     * @param string $path
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function checkImage(string $path)
    {
        return $this->httpUpload('img_sec_check', ['media' => $path]);
    }
}