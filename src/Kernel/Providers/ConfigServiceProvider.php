<?php
namespace Sinta\Wechat\Kernel\Providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

use Sinta\Wechat\Kernel\Config;

/**
 * 配置服务
 *
 * Class ConfigServiceProvider
 * @package Sinta\Wechat\Kernel\Providers
 */
class ConfigServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['config'] = function ($app) {
            return new Config($app->getConfig());
        };
    }
}