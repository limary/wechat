<?php
namespace Sinta\Wechat\OfficialAccount\TemplateMessage;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 公众号-模板消息服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount\TemplateMessage
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['template_message'] = function ($app) {
            return new Client($app);
        };
    }
}