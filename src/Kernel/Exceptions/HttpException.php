<?php
namespace Sinta\Wechat\Kernel\Exceptions;

use Psr\Http\Message\ResponseInterface;

class HttpException extends Exception
{
    /**
     * @var \Psr\Http\Message\ResponseInterface|null
     */
    public $response;

    /**
     *
     * HttpException constructor.
     * @param string $message
     * @param ResponseInterface|null $response
     * @param null $code
     */
    public function __construct($message, ResponseInterface $response = null, $code = null)
    {
        parent::__construct($message, $code);

        $this->response = $response;
        if($response){
            $response->getBody()->rewind();
        }
    }
}