<?php

namespace Sinta\Wechat\Payment\Refund;

use Sinta\Wechat\Payment\Kernel\BaseClient;

/**
 *退款处理
 *
 * Class Client
 * @package Sinta\Wechat\Payment\Refund
 */
class Client extends BaseClient
{
    /**
     * 通过订单号发起退款
     *
     * @param string $number
     * @param string $refundNumber
     * @param int $totalFee
     * @param int $refundFee
     * @param array $optional
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|BaseClient
     */
    public function byOutTradeNumber(string $number,string $refundNumber,int $totalFee, int $refundFee, array $optional = [])
    {
        return $this->refund($refundNumber, $totalFee, $refundFee, array_merge($optional, ['out_trade_no' => $number]));
    }

    /**
     * 通过交易号发起退款
     *
     * @param string $transactionId
     * @param string $refundNumber
     * @param int $totalFee
     * @param int $refundFee
     * @param array $optional
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|BaseClient
     */
    public function byTransactionId(string $transactionId, string $refundNumber, int $totalFee, int $refundFee, array $optional = [])
    {
        return $this->refund($refundNumber, $totalFee, $refundFee, array_merge($optional, ['transaction_id' => $transactionId]));
    }

    /**
     * 通过易号发起退款交易查询
     *
     * @param string $transactionId
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function queryByTransactionId(string $transactionId)
    {
        return $this->query($transactionId, 'transaction_id');
    }


    public function queryByOutTradeNumber(string $outTradeNumber)
    {
        return $this->query($outTradeNumber, 'out_trade_no');
    }

    public function queryByOutRefundNumber(string $outRefundNumber)
    {
        return $this->query($outRefundNumber, 'out_refund_no');
    }

    public function queryByRefundId(string $refundId)
    {
        return $this->query($refundId, 'refund_id');
    }


    protected function refund(string $refundNumber, int $totalFee, int $refundFee, $optional = [])
    {
        $params = array_merge([
            'out_refund_no' => $refundNumber,
            'total_fee' => $totalFee,
            'refund_fee' => $refundFee,
            'appid' => $this->app['config']->app_id,
        ], $optional);

        return $this->safeRequest($this->wrap(
            $this->app->inSandbox() ? 'pay/refund' : 'secapi/pay/refund'
        ), $params);
    }

    protected function query(string $number, string $type)
    {
        $params = [
            'appid' => $this->app['config']->app_id,
            $type => $number,
        ];

        return $this->request('pay/refundquery', $params);
    }
}