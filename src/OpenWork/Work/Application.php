<?php
namespace Sinta\Wechat\OpenWork\Work;

use Sinta\Wechat\OpenWork\Application as OpenWork;
use Sinta\Wechat\OpenWork\Work\Auth\AccessToken;
use Sinta\Wechat\Work\Application as Work;

class Application extends Work
{

    public function __construct(string $authCorpId,string $permanentCode, OpenWork $component, array $prepends = [])
    {
        parent::__construct($component->getConfig(), $prepends +[
                'access_token' => function($app) use($authCorpId, $permanentCode,$component){
                    return new AccessToken($app, $authCorpId, $permanentCode, $component);
                },

            ]);
    }
}