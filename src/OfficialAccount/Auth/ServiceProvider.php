<?php
namespace Sinta\Wechat\OfficialAccount\Auth;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 公众号认证服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount\Auth
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        !isset($app['access_token']) && $app['access_token'] = function ($app){
            return new AccessToken($app);
        };
    }
}