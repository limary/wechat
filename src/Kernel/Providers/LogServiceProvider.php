<?php
/**
 * Created by PhpStorm.
 * User: limary
 * Date: 18-4-13
 * Time: 下午1:58
 */

namespace Sinta\Wechat\Kernel\Providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Sinta\Wechat\Kernel\Log\LogManager;

/**
 * 日志服务
 *
 * Class LogServiceProvider
 * @package Sinta\Wechat\Kernel\Providers
 */
class LogServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['logger'] = $pimple['log'] = function ($app) {
            $config = $this->formatLogConfig($app);
            if (!empty($config)) {
                $app['config']->merge($config);
            }
            return new LogManager($app);
        };
    }

    public function formatLogConfig($app)
    {
        if (empty($app['config']->get('log'))) {
            return [
                'log' => [
                    'default' => 'errorlog',
                    'channels' => [
                        'errorlog' => [
                            'driver' => 'errorlog',
                            'level' => 'debug',
                        ],
                    ],
                ],
            ];
        }
        // 4.0 version
        if (empty($app['config']->get('log.driver'))) {
            return [
                'log' => [
                    'default' => 'single',
                    'channels' => [
                        'single' => [
                            'driver' => 'single',
                            'path' => $app['config']->get('log.file') ?: \sys_get_temp_dir().'/logs/easywechat.log',
                            'level' => $app['config']->get('log.level', 'debug'),
                        ],
                    ],
                ],
            ];
        }
        $name = $app['config']->get('log.driver');
        return [
            'log' => [
                'default' => $name,
                'channels' => [
                    $name => $app['config']->get('log'),
                ],
            ],
        ];
    }
}