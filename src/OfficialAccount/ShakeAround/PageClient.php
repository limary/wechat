<?php
namespace Sinta\Wechat\OfficialAccount\ShakeAround;

use Sinta\Wechat\Kernel\Client as BaseClient;

class PageClient extends BaseClient
{
    public function create(array $data)
    {
        return $this->httpPostJson('shakearound/page/add', $data);
    }

    public function update(int $pageId, array $data)
    {
        return $this->httpPostJson('shakearound/page/update', array_merge(['page_id' => $pageId], $data));
    }

    public function listByIds(array $pageIds)
    {
        $params = [
            'type' => 1,
            'page_ids' => $pageIds,
        ];

        return $this->httpPostJson('shakearound/page/search', $params);
    }

    public function list(int $begin, int $count)
    {
        $params = [
            'type' => 2,
            'begin' => $begin,
            'count' => $count,
        ];

        return $this->httpPostJson('shakearound/page/search', $params);
    }

    public function delete(int $pageId)
    {
        $params = [
            'page_id' => $pageId,
        ];

        return $this->httpPostJson('shakearound/page/delete', $params);
    }
}