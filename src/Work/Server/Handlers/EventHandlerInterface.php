<?php
namespace Sinta\Wechat\Work\Server\Handlers;

use Sinta\Wechat\Kernel\Contracts\EventHandlerInterface;
use Sinta\Wechat\Kernel\Decorators\FinallyResult;
use Sinta\Wechat\Kernel\ServiceContainer;

class EchoStrHandler implements EventHandlerInterface
{
    protected $app;

    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;
    }

    /**
     * 处理事件
     *
     * @param array $payload
     * @return mixed|FinallyResult
     */
    public function handle($payload = null)
    {
        if ($decrypted = $this->app['request']->get('echostr')) {
            $str = $this->app['encryptor']->decrypt(
                $decrypted,
                $this->app['request']->get('msg_signature'),
                $this->app['request']->get('nonce'),
                $this->app['request']->get('timestamp')
            );
            return new FinallyResult($str);
        }
    }
}