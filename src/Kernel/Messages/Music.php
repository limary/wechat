<?php
namespace Sinta\Wechat\Kernel\Messages;

/**
 * 音乐消息
 *
 * Class Music
 * @package Sinta\Wechat\Kernel\Messages
 */
class Music extends Message
{
    protected $type = 'music';

    protected $properties = [
        'title',
        'description',
        'url',
        'hq_url',
        'thumb_media_id',
        'format'
    ];

    protected $jsonAliases = [
        'musicurl' => 'url',
        'hqmusicurl' => 'hq_url',
    ];


    public function toXmlArray()
    {
        $music = [
            'Music' => [
                'Title' => $this->get('title'),
                'Description' => $this->get('description'),
                'MusicUrl' => $this->get('url'),
                'HQMusicUrl' => $this->get('hq_url'),
            ],
        ];
        if ($thumbMediaId = $this->get('thumb_media_id')) {
            $music['Music']['ThumbMediaId'] = $thumbMediaId;
        }
        return $music;
    }
}