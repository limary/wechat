<?php

namespace Sinta\Wechat\Tests\MiniProgram\Sns;

use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\MiniProgram\Auth\Client;
use Sinta\Wechat\Tests\TestCase;

class AuthTest extends TestCase
{
    public function testGetSessionKey()
    {
        $client = $this->mockApiClient(Client::class, [], new ServiceContainer(['app_id' => 'app-id', 'secret' => 'mock-secret']));

        $client->expects()->httpGet('sns/jscode2session', [
            'appid' => 'app-id',
            'secret' => 'mock-secret',
            'js_code' => 'js-code',
            'grant_type' => 'authorization_code',
        ])->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->session('js-code'));
    }
}
