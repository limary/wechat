<?php
namespace Sinta\Wechat\Work\OA;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    /**
     * 获取打卡数据
     *
     * @param int $startTime
     * @param int $endTime
     * @param array $userList
     * @param int $type
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function checkinRecords(int $startTime, int $endTime, array $userList, int $type = 3)
    {
        $params = [
            'opencheckindatatype' => $type,
            'starttime' => $startTime,
            'endtime' => $endTime,
            'useridlist' => $userList,
        ];
        return $this->httpPostJson('cgi-bin/checkin/getcheckindata', $params);
    }

    /**
     *获取审批数据
     *
     * @param int $startTime
     * @param int $endTime
     * @param int|null $nextNumber
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function approvalRecords(int $startTime,int $endTime,int $nextNumber = null)
    {
        $params = [
            'starttime' => $startTime,
            'endtime' => $endTime,
            'next_spnum' => $nextNumber,
        ];
        return $this->httpPostJson('cgi-bin/corp/getapprovaldata', $params);
    }


    /**
     * 用户列表不超过100个，若用户超过100个，请分批获取。
     * 用户在不同日期的规则不一定相同，请按天获取。
     *
     * @param int $datetime
     * @param array $userList 需要获取打卡规则的用户列表
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function checkinOption(int $datetime, array $userList)
    {
        $params = [
            'datetime' => $datetime,
            'useridlist' => $userList
        ];
        return $this->httpPostJson('cgi-bin/checkin/getcheckinoption', $params);
    }
}