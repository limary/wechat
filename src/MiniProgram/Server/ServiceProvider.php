<?php
namespace Sinta\Wechat\MiniProgram\Server;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Sinta\Wechat\MiniProgram\Encryptor;
use Sinta\Wechat\OfficialAccount\Server\Guard;
use Sinta\Wechat\OfficialAccount\Server\Handlers\EchoStrHandler;

/**
 * 服务供应
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\MiniProgram\Server
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        !isset($app['encryptor']) && $app['encryptor'] = function ($app){
            return new Encryptor(
                $app['config']['app_id'],
                $app['config']['token'],
                $app['config']['aes_key']
            );
        };

        !isset($app['server']) && $app['server'] = function ($app){
            $guard = new Guard($app);
            $guard->push(new EchoStrHandler($app));

            return $guard;
        };
    }
}