<?php
namespace Sinta\Wechat\Payment\Kernel;

use Sinta\Wechat\Kernel\Support;
use Sinta\Wechat\Kernel\Traits\HasHttpRequests;
use Sinta\Wechat\Payment\Application;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;

/**
 * 请求客户端
 *
 * Class BaseClient
 * @package Sinta\Wechat\Payment\Kernel
 */
class BaseClient
{

    use HasHttpRequests { request as performRequest; }

    protected $app;

    /**
     * 创建请求客户端实例
     *
     * BaseClient constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->setHttpClient($this->app['http_client']);
    }

    protected function prepends()
    {
        return [];
    }

    /**
     * 发送请求
     *
     * @param string $endpoint
     * @param array $params
     * @param string $method
     * @param array $options
     * @param bool $returnResponse
     * @return array|mixed|null|ResponseInterface|static
     * @throws \Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Sinta\Wechat\Kernel\Exceptions\InvalidConfigException
     */
    protected function request(string $endpoint, array $params = [], $method = 'post',
                               array $options = [], $returnResponse = false)
    {
        $base = [
            'mch_id' => $this->app['config']['mch_id'],
            'nonce_str' => uniqid(),
            'sub_mch_id' => $this->app['config']['sub_mch_id'],
            'sub_appid' => $this->app['config']['sub_appid'],
        ];
        $params = array_filter(array_merge($base, $this->prepends(), $params));

        $secretKey = $this->app->getKey($endpoint);
        if ('HMAC-SHA256' === ($params['sign_type'] ?? 'MD5')) {
            $encryptMethod = function ($str) use ($secretKey) {
                return hash_hmac('sha256', $str, $secretKey);
            };
        } else {
            $encryptMethod = 'md5';
        }
        $params['sign'] = Support\generate_sign($params, $secretKey, $encryptMethod);


        $options = array_merge([
            'body' => Support\XML::build($params),
        ], $options);
        $this->pushMiddleware($this->logMiddleware(),'log');

        $response = $this->performRequest($endpoint, $method, $options);

        return $returnResponse
            ? $response
            : $this->castResponseToType($response, $this->app->config->get('response_type'));
    }


    protected function logMiddleware()
    {
        $formatter = new MessageFormatter($this->app['config']['http.log_template'] ?? MessageFormatter::DEBUG);
        return Middleware::log($this->app['logger'], $formatter);
    }

    /**
     * 返回请求返回原示响应
     *
     * @param $endpoint
     * @param array $params
     * @param string $method
     * @param array $options
     * @return array|mixed|null|ResponseInterface|BaseClient
     * @throws \Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Sinta\Wechat\Kernel\Exceptions\InvalidConfigException
     */
    protected function requestRaw($endpoint, array $params = [], $method = 'post', array $options = [])
    {
        return $this->request($endpoint, $params, $method, $options, true);
    }

    /**
     * 安全请求
     *
     * @param $endpoint
     * @param array $params
     * @param string $method
     * @param array $options
     * @return array|mixed|null|ResponseInterface|BaseClient
     * @throws \Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Sinta\Wechat\Kernel\Exceptions\InvalidConfigException
     */
    protected function safeRequest($endpoint, array $params, $method = 'post',array $options = [])
    {
        $options = array_merge([
            'cert' => $this->app['config']->get('cert_path'),
            'ssl_key' => $this->app['config']->get('key_path'),
        ], $options);

        return $this->request($endpoint, $params, $method, $options);
    }

    /**
     * 获取key值
     *
     * @param string $endpoint
     * @return mixed
     */
    protected function wrap(string $endpoint): string
    {
        return $this->app->inSandbox() ? "sandboxnew/{$endpoint}" : $endpoint;
    }

}