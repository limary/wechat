<?php
namespace Sinta\Wechat\Work\User;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 *管理标签
 *
 * Class TagClient
 * @package Sinta\Wechat\Work\User
 * @see http://qydev.weixin.qq.com/wiki/index.php?title=管理标签
 */
class TagClient extends BaseClient
{
    /**
     * 创建标签
     *
     * @param string $tagName 标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。
     * @param int|null $tagId 标签id，整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(string $tagName, int $tagId = null)
    {
        $params = [
            'tagname' => $tagName,
            'tagid' => $tagId,
        ];
        return $this->httpPostJson('cgi-bin/tag/create', $params);
    }

    /**
     * 更新标签名字
     *
     * @param int $tagId
     * @param string $tagName  标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update(int $tagId, string $tagName)
    {
        $params = [
            'tagid' => $tagId,
            'tagname' => $tagName,
        ];
        return $this->httpPostJson('cgi-bin/tag/update', $params);
    }

    /**
     * 删除标签
     *
     * @param int $tagId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(int $tagId)
    {
        return $this->httpGet('cgi-bin/tag/delete', ['tagid' => $tagId]);
    }

    /**
     * 获取标签成员
     *
     * @param int $tagId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get(int $tagId)
    {
        return $this->httpGet('cgi-bin/tag/get', ['tagid' => $tagId]);
    }

    /**
     * 增加标签成员
     *
     * @param int $tagId
     * @param array $userList
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function tagUsers(int $tagId, array $userList = [])
    {
        return $this->tagOrUntagUsers('cgi-bin/tag/addtagusers', $tagId, $userList);
    }

    /**
     * 增加标签成员
     *
     * @param int $tagId
     * @param array $partyList
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function tagDepartments(int $tagId, array $partyList = [])
    {
        return $this->tagOrUntagUsers('cgi-bin/tag/addtagusers', $tagId, [], $partyList);
    }

    /**
     * 删除标签成员
     *
     * @param int $tagId
     * @param array $userList
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function untagUsers(int $tagId, array $userList = [])
    {
        return $this->tagOrUntagUsers('cgi-bin/tag/deltagusers', $tagId, $userList);
    }


    public function untagDepartments(int $tagId, array $partyList = [])
    {
        return $this->tagOrUntagUsers('cgi-bin/tag/deltagusers', $tagId, [], $partyList);
    }

    protected function tagOrUntagUsers(string $endpoint, int $tagId, array $userList = [], array $partyList = [])
    {
        $data = [
            'tagid' => $tagId,
            'userlist' => $userList,
            'partylist' => $partyList,
        ];
        return $this->httpPostJson($endpoint, $data);
    }


    /**
     * 获取标签列表
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list()
    {
        return $this->httpGet('cgi-bin/tag/list');
    }

}