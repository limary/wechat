<?php

namespace Sinta\Wechat\OpenPlatform\CodeTemplate;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 模板服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OpenPlatform\CodeTemplate
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['code_template'] = function ($app) {
            return new Client($app);
        };
    }
}