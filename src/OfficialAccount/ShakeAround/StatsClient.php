<?php
namespace Sinta\Wechat\OfficialAccount\ShakeAround;

use Sinta\Wechat\Kernel\Client as BaseClient;

class StatsClient extends BaseClient
{
    public function deviceSummary(array $deviceIdentifier, int $beginTime, int $endTime)
    {
        $params = [
            'device_identifier' => $deviceIdentifier,
            'begin_date' => $beginTime,
            'end_date' => $endTime,
        ];

        return $this->httpPostJson('shakearound/statistics/device', $params);
    }

    public function devicesSummary(int $timestamp, int $pageIndex)
    {
        $params = [
            'date' => $timestamp,
            'page_index' => $pageIndex,
        ];

        return $this->httpPostJson('shakearound/statistics/devicelist', $params);
    }

    public function pageSummary(int $pageId, int $beginTime, int $endTime)
    {
        $params = [
            'page_id' => $pageId,
            'begin_date' => $beginTime,
            'end_date' => $endTime,
        ];

        return $this->httpPostJson('shakearound/statistics/page', $params);
    }

    public function pagesSummary(int $timestamp, int $pageIndex)
    {
        $params = [
            'date' => $timestamp,
            'page_index' => $pageIndex,
        ];

        return $this->httpPostJson('shakearound/statistics/pagelist', $params);
    }
}