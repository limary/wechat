<?php
namespace Sinta\Wechat\MiniProgram\Auth;

use Sinta\Wechat\Kernel\Client as BaseClient;


/**
 * code换session
 *
 * Class Client
 * @package Sinta\Wechat\MiniProgram\Auth
 */
class Client extends BaseClient
{

    public function session(string $code)
    {
        $params = [
            'appid' => $this->app['config']['app_id'],
            'secret' => $this->app['config']['secret'],
            'js_code' => $code,
            'grant_type' => 'authorization_code',
        ];

        return $this->httpGet('sns/jscode2session',$params);
    }
}