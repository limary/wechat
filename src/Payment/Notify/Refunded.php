<?php

namespace Sinta\Wechat\Payment\Notify;

use Closure;
use Sinta\Wechat\Kernel\Support\XML;

/**
 *退款通知处理
 *
 * Class Refunded
 * @package Sinta\Wechat\Payment\Notify
 */
class Refunded extends Handler
{
    protected $check = false;

    public function handle(Closure $closure)
    {
        $this->strict(
            call_user_func($closure,$this->getMessage(), [$this, 'fail'])
        );

        return $this->toResponse();
    }


    public function reqInfo()
    {
        return XML::parse($this->decryptMessage('req_info'));
    }
}