<?php
namespace Sinta\Wechat\BasicService\Media;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 素材服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\BaseService\Media
 */
class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
       $app['media'] = function ($app){
            return new Client($app);
       };
    }
}