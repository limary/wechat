<?php
namespace Sinta\Wechat\OfficialAccount\User;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 用户管理服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount\User
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['user'] = function ($app) {
            return new UserClient($app);
        };
        $app['user_tag'] = function ($app) {
            return new TagClient($app);
        };
    }
}