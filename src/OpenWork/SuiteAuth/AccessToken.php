<?php
namespace Sinta\Wechat\OpenWork\SuiteAuth;

use  Sinta\Wechat\Kernel\AccessToken as BaseAccessToken;

class AccessToken extends BaseAccessToken
{

    protected $requestMethod = 'POST';

    protected $endpointToGetToken = 'cgi-bin/service/get_suite_token';

    protected $tokenKey = 'suite_access_token';

    protected $cachePrefix = 'sintawechat.kernel.suite_access_token.';

    protected function getCredentials(): array
    {
        return [
            'suite_id' => $this->app['config']['suite_id'],
            'suite_secret' => $this->app['config']['suite_secret'],
            'suite_ticket' => $this->app['suite_ticket']->getTicket(),
        ];
    }
}