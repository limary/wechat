<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\Server;

use Sinta\Wechat\Kernel\ServerGuard;

class Guard extends ServerGuard
{
    protected function getToken()
    {
        return $this->app['encryptor']->getToken();
    }
}