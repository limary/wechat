<?php

namespace Sinta\Wechat\OpenWork;


use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\OpenWork\Work\Application as Work;

class Application extends ServiceContainer
{
    protected $providers = [
        Auth\ServiceProvider::class,
        SuiteAuth\ServiceProvider::class,
        Server\ServiceProvider::class,
        Corp\ServiceProvider::class,
        Provider\ServiceProvider::class,
    ];


    public function work(string $authCorpId, string $permanentCode)
    {
        return new Work($authCorpId, $permanentCode, $this);
    }


    public function __call($method, $arguments)
    {
        return $this['base']->$method(...$arguments);
    }
}