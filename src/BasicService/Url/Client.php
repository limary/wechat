<?php
namespace Sinta\Wechat\BasicService\Url;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    protected $baseUri = 'https://api.weixin.qq.com/';

    public function shorten(string $url)
    {
        $params = [
            'action' => 'long2short',
            'long_url' => $url,
        ];

        return $this->httpPostJson('cgi-bin/shorturl', $params);
    }
}