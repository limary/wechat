<?php
namespace Sinta\Wechat\MiniProgram\TemplateMessage;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 模板消息服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\MiniProgram\TemplateMessage
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['template_message'] = function ($app) {
            return new Client($app);
        };
    }
}