<?php
namespace Sinta\Wechat\OfficialAccount\Card;


use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;


class Card extends Client
{
    /**
     * @param $property
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function __get($property)
    {
        if (isset($this->app["card.{$property}"])) {
            return $this->app["card.{$property}"];
        }

        throw new InvalidArgumentException(sprintf('No card service named "%s".', $property));
    }
}