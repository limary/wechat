<?php
namespace Sinta\Wechat\Work;


use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\Work\MiniProgram\Application as MiniProgram;

class Application extends ServiceContainer
{

    protected $providers = [
        OA\ServiceProvider::class,
        Auth\ServiceProvider::class,
        Base\ServiceProvider::class,
        Menu\ServiceProvider::class,
        OAuth\ServiceProvider::class,
        User\ServiceProvider::class,
        Agent\ServiceProvider::class,
        Media\ServiceProvider::class,
        Message\ServiceProvider::class,
        Department\ServiceProvider::class,
        Server\ServiceProvider::class,
        Jssdk\ServiceProvider::class,
        Invoice\ServiceProvider::class,
        Chat\ServiceProvider::class,
        Crm\ServiceProvider::class,
    ];

    public function miniProgram(): MiniProgram
    {
        return new MiniProgram($this->getConfig());
    }

    protected $defaultConfig = [
        'http' => [
            'base_uri' => 'https://qyapi.weixin.qq.com/'
        ],
    ];

    public function __call($method,$arguments)
    {
        return $this['base']->$method(...$arguments);
    }
}