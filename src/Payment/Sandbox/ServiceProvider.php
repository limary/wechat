<?php

namespace Sinta\Wechat\Payment\Sandbox;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 沙箱服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\Payment\Sandbox
 */
class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['sandbox'] = function ($app) {
            return new Client($app);
        };
    }
}