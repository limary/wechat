<?php
namespace Sinta\Wechat\OfficialAccount\Menu;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 自定义菜单
 *
 * Class Client
 * @package Sinta\Wechat\OfficialAccount\Menu
 * @https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141013
 */
class Client extends BaseClient
{
    /**
     * 获取所有菜单
     *
     * 用接口创建自定义菜单后，开发者还可使用接口查询自定义菜单的结构。
     * 另外请注意，在设置了个性化菜单后，使用本自定义菜单查询接口可以获取默认菜单和全部个性化菜单信息
     * @return mixed
     */
    public function list()
    {
        return $this->httpGet('cgi-bin/menu/get');
    }

    /**
     * 获取当前菜单
     *
     * @return mixed
     */
    public function current()
    {
        return $this->httpGet('cgi-bin/get_current_selfmenu_info');
    }


    /**
     * 添加一个菜单
     *
     * @param array $buttons
     * @param array $matchRule 空时,创建个性化菜单
     * @return mixed
     */
    public function create(array $buttons, array $matchRule = [])
    {
        if (!empty($matchRule)) {
            return $this->httpPostJson('cgi-bin/menu/addconditional', [
                'button' => $buttons,
                'matchrule' => $matchRule,
            ]);
        }

        return $this->httpPostJson('cgi-bin/menu/create', ['button' => $buttons]);
    }


    /**
     * 删除菜单
     *
     * 使用接口创建自定义菜单后，开发者还可使用接口删除当前使用的自定义菜单。
     * 另请注意，在个性化菜单时，调用此接口会删除默认菜单及全部个性化菜单。
     * @param int|null $menuId 存在,删除个性化菜单
     * @return mixed
     */
    public function delete(int $menuId = null)
    {
        if (is_null($menuId)) {
            return $this->httpGet('cgi-bin/menu/delete');
        }

        return $this->httpPostJson('cgi-bin/menu/delconditional', ['menuid' => $menuId]);
    }

    /**
     * 测试个性化菜单匹配结果
     *
     * @param string $userId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function match(string $userId)
    {
        return $this->httpPostJson('cgi-bin/menu/trymatch', ['user_id' => $userId]);
    }
}