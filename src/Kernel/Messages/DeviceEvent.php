<?php
namespace Sinta\Wechat\Kernel\Messages;


class DeviceEvent extends Message
{
    protected $type = 'device_event';

    protected $properties = [
        'device_type',
        'device_id',
        'content',
        'session_id',
        'open_id',
    ];
}