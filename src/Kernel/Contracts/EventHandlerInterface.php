<?php
namespace Sinta\Wechat\Kernel\Contracts;

/**
 * 事件处理接口
 *
 * Interface IEventHandler
 * @package Sinta\Wechat\Kernel\Contract
 */
interface EventHandlerInterface
{
    /**
     * 处理器
     *
     * @param array $payload
     * @return mixed
     */
    public function handle($payload = null);
}