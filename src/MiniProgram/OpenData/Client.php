<?php
namespace Sinta\Wechat\MiniProgram\OpenData;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{

    protected $baseUri = 'https://api.weixin.qq.com/wxa/';


    public function removeUserStorage(string $openid, string $sessionKey, array $key)
    {
        $data = ['key' => $key];
        $query = [
            'appid' => $this->app['config']['app_id'],
            'sig_method' => 'hmac_sha256',
            'signature' => hash_hmac('sha256', json_encode($data), $sessionKey),
        ];
        return $this->httpPostJson('remove_user_storage', $data, $query);
    }


    public function setUserStorage(string $openid, string $sessionKey, array $kvList)
    {
        $kvList = $this->formatKVLists($kvList);

        $data = ['kv_list' => $kvList];
        $query = [
            'appid' => $this->app['config']['app_id'],
            'secret' => $this->app['config']['secret'],
            'openid' => $openid,
            'sig_method' => 'hmac_sha256',
            'signature' => hash_hmac('sha256', json_encode($data), $sessionKey),
        ];
        return $this->httpPostJson('set_user_storage', $data, $query);
    }

    protected function formatKVLists(array $params)
    {
        $formatted = [];
        foreach ($params as $name => $value) {
            $formatted[] = [
                'key' => $name,
                'value' => strval($value),
            ];
        }
        return $formatted;
    }

}