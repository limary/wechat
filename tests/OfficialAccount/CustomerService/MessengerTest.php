<?php

namespace Sinta\Wechat\Tests\OfficialAccount\CustomerService;

use Sinta\Wechat\Kernel\Exceptions\RuntimeException;
use Sinta\Wechat\Kernel\Messages\Raw;
use Sinta\Wechat\Kernel\Messages\Text;
use Sinta\Wechat\OfficialAccount\CustomerService\Client;
use Sinta\Wechat\OfficialAccount\CustomerService\Messenger;
use Sinta\Wechat\Tests\TestCase;

class MessengerTest extends TestCase
{
    public function testSendWithoutMessage()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('No message to send.');

        $messenger = new Messenger(\Mockery::mock(Client::class));
        $messenger->send();
    }

    public function testSend()
    {
        $client = \Mockery::mock(Client::class);

        // without by
        $client->expects()->send([
            'touser' => 'mock-openid',
            'msgtype' => 'text',
            'text' => ['content' => 'text message.'],
        ]);
        $messenger = new Messenger($client);
        $messenger->message('text message.')->to('mock-openid');
        $messenger->send();

        // with from
        $client->expects()->send([
            'touser' => 'mock-openid',
            'msgtype' => 'text',
            'customservice' => [
                'kf_account' => 'overtrue@test',
            ],
            'text' => ['content' => 'text message.'],
        ]);
        $messenger = new Messenger($client);
        $messenger->message('text message.')->to('mock-openid')->from('overtrue@test');
        $messenger->send();

        // property access
        $this->assertInstanceOf(Text::class, $messenger->message);
        $this->assertSame('mock-openid', $messenger->to);
        $this->assertNull($messenger->not_exists_property);
    }

    public function testSendWithRawMessage()
    {
        $client = \Mockery::mock(Client::class);

        $message = new Raw(json_encode([
            'touser' => 'mock-openid',
            'msgtype' => 'text',
            'text' => ['content' => 'text message.'],
        ]));

        $client->expects()->send([
            'touser' => 'mock-openid',
            'msgtype' => 'text',
            'text' => ['content' => 'text message.'],
        ]);
        $messenger = new Messenger($client);
        $messenger->message($message)->to('mock-openid');
        $messenger->send();
    }
}
