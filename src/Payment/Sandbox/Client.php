<?php

namespace Sinta\Wechat\Payment\Sandbox;


use Sinta\Wechat\Kernel\Traits\InteractsWithCache;
use Sinta\Wechat\Payment\Kernel\BaseClient;
use Sinta\Wechat\Payment\Kernel\Exceptions\SandboxException;

/**
 * 沙箱方法
 *
 * Class Client
 * @https://pay.weixin.qq.com/wiki/doc/api/native.php?chapter=23_1
 * @package Sinta\Wechat\Payment\Sandbox
 */
class Client extends BaseClient
{
    use InteractsWithCache;

    const ENDPOINT = '/sandboxnew/pay/getsignkey';


    /**
     * @return string
     * @throws SandboxException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getKey(): string
    {
        if ($cache = $this->getCache()->get($this->getCacheKey())) {
            return $cache;
        }

        $response = $this->request(self::ENDPOINT);

        if ($response['return_code'] === 'SUCCESS') {
            $this->getCache()->set($this->getCacheKey(), $key = $response['sandbox_signkey'], 24 * 3600);

            return $key;
        }

        throw new SandboxException($response['retmsg'] ?? $response['return_msg']);
    }

    protected function getCacheKey()
    {
        return 'sinta.wechat.payment.sandbox.'.$this->app['config']->app_id.$this->app['config']['mch_id'];
    }

}