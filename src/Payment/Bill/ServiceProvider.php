<?php
namespace Sinta\Wechat\Payment\Bill;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 下载对账单
 *
 * Class ServiceProvider
 * @see https://pay.weixin.qq.com/wiki/doc/api/micropay_sl_jw.php?chapter=9_6
 * @package Sinta\Wechat\Payment\Bill
 */
class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['bill'] = function ($app){
            return new Client($app);
        };
    }
}