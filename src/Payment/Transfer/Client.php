<?php
namespace Sinta\Wechat\Payment\Transfer;

use Sinta\Wechat\Payment\Kernel\BaseClient;
use Sinta\Wechat\Kernel\Exceptions\RuntimeException;
use function Sinta\Wechat\Kernel\Support\get_server_ip;
use function Sinta\Wechat\Kernel\Support\rsa_public_encrypt;

class Client extends BaseClient
{
    /**
     * @param string $partnetTradeNo
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|BaseClient
     */
    public function queryBalanceOrder(string $partnetTradeNo)
    {
        $params = [
            'appid' => $this->app['config']->app_id,
            'mch_id' => $this->app['config']->mch_id,
            'partner_trade_no' => $partnetTradeNo,
        ];

        return $this->safeRequest('mmpaymkttransfers/gettransferinfo', $params);
    }


    public function toBalance(array $params)
    {
        $base = [
            'mch_id' => null,
            'mchid' => $this->app['config']->mch_id,
            'mch_appid' => $this->app['config']->app_id,
        ];
        if (empty($params['spbill_create_ip'])) {
            $params['spbill_create_ip'] = get_server_ip();
        }
        return $this->safeRequest('mmpaymkttransfers/promotion/transfers', array_merge($base, $params));
    }

    public function queryBankCardOrder(string $partnetTradeNo)
    {
        $params = [
            'mch_id' => $this->app['config']->mch_id,
            'partner_trade_no' => $partnetTradeNo,
        ];
        return $this->safeRequest('mmpaysptrans/query_bank', $params);
    }

    /**
     * @param array $params
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|BaseClient
     * @throws RuntimeException
     */
    public function toBankCard(array $params)
    {
        foreach (['bank_code', 'partner_trade_no', 'enc_bank_no', 'enc_true_name', 'amount'] as $key) {
            if (empty($params[$key])) {
                throw new RuntimeException(sprintf('"%s" is required.', $key));
            }
        }

        $publicKey = file_get_contents($this->app['config']->get('rsa_public_key_path'));
        $params['enc_bank_no'] = rsa_public_encrypt($params['enc_bank_no'], $publicKey);
        $params['enc_true_name'] = rsa_public_encrypt($params['enc_true_name'], $publicKey);

        return $this->safeRequest('mmpaysptrans/pay_bank', $params);
    }
}