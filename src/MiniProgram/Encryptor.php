<?php
namespace Sinta\Wechat\MiniProgram;

use Sinta\Wechat\Kernel\Encryptor as BaseEncryptor;
use Sinta\Wechat\Kernel\Support\AES;


/**
 * 解析器
 *
 * Class Encryptor
 * @package Sinta\Wechat\MiniProgram
 */
class Encryptor extends BaseEncryptor
{
    /**
     * 解密数据
     *
     * @param string $sessionKey
     * @param string $iv
     * @param string $encrypted
     * @return array
     */
    public function decryptData(string $sessionKey,string $iv,string $encrypted):array
    {
        $decrypted = AES::decrypt(
            base64_decode($encrypted,false), base64_decode($sessionKey, false), base64_decode($iv, false)
        );

        $decrypted = json_decode($this->pkcs7Unpad($decrypted), true);

        if (!$decrypted) {
            throw new DecryptException('The given payload is invalid.');
        }

        return $decrypted;
    }
}