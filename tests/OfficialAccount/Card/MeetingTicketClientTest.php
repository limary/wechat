<?php

namespace Sinta\Wechat\Tests\OfficialAccount\Card;

use Sinta\Wechat\OfficialAccount\Card\MeetingTicketClient;
use Sinta\Wechat\Tests\TestCase;

class MeetingTicketClientTest extends TestCase
{
    public function testUpdateUser()
    {
        $client = $this->mockApiClient(MeetingTicketClient::class);

        $params = [
            'foo' => 'bar',
        ];
        $client->expects()->httpPostJson('card/meetingticket/updateuser', $params)->andReturn('mock-result')->once();

        $this->assertSame('mock-result', $client->updateUser($params));
    }
}
