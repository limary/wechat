<?php
namespace Sinta\Wechat\MiniProgram\AppCode;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Http\StreamResponse;

/**
 *应用的二级码
 *
 * Class Client
 * @package Sinta\Wechat\MiniProgram\AppCode
 * @see https://mp.weixin.qq.com/debug/wxadoc/dev/api/qrcode.html
 */
class Client extends BaseClient
{
    /**
     * 获取AppCode
     *
     * @param string $path 不能为空，最大长度 128 字节
     * @param array $optional  二维码的宽度
     * @return static
     * 注意：通过该接口生成的小程序码，永久有效，数量限制见文末说明，请谨慎使用。用户扫描该码进入小程序后，将直接进入 path 对应的页面。
     */
    public function get(string $path, array $optional = [])
    {
        $params = array_merge([
            'path' => $path,
        ], $optional);
        return $this->getStream('wxa/getwxacode', $params);
    }

    /**
     *
     * 适用于需要的码数量极多，或仅临时使用的业务场景
     *
     * @param string $scene 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
     * @param array $optional
     * @return static
     *
     */
    public function getUnlimit(string $scene, array $optional = [])
    {
        $params = array_merge([
            'scene' => $scene,
        ], $optional);
        return $this->getStream('wxa/getwxacodeunlimit', $params);
    }

    /**
     * 适用于需要的码数量较少的业务场景
     *
     * @param string $path
     * @param int|null $width
     * @return static
     */
    public function getQrCode(string $path, int $width = null)
    {
        return $this->getStream('cgi-bin/wxaapp/createwxaqrcode', compact('path', 'width'));
    }


    protected function getStream(string $endpoint, array $params)
    {
        return StreamResponse::buildFromPsrResponse($this->requestRaw($endpoint, 'POST', ['json' => $params]));
    }

}