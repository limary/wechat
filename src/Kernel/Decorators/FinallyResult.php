<?php
namespace Sinta\Wechat\Kernel\Decorators;

class FinallyResult
{

    public $content;

    public function __construct($content)
    {
        $this->content = $content;
    }
}