<?php
namespace Sinta\Wechat\OfficialAccount\Semantic;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 语义解析服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount\Semantic
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['semantic'] = function ($app) {
            return new Client($app);
        };
    }
}