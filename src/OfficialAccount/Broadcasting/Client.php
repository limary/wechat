<?php
namespace Sinta\Wechat\OfficialAccount\Broadcasting;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Contracts\MessageInterface;
use Sinta\Wechat\Kernel\Exceptions\RuntimeException;
use Sinta\Wechat\Kernel\Messages\Card;
use Sinta\Wechat\Kernel\Messages\Image;
use Sinta\Wechat\Kernel\Messages\Media;
use Sinta\Wechat\Kernel\Messages\Text;
use Sinta\Wechat\Kernel\Support\Arr;


class Client extends BaseClient
{
    const PREVIEW_BY_OPENID = 'touser';
    const PREVIEW_BY_NAME = 'towxname';

    /**
     * 发送一个消息
     *
     * @param array $message
     * @return \Psr\Http\Message\ResponseInterface|\Sinta\Wechat\Kernel\Support\Collection|array|object|string
     */
    public function send(array $message)
    {
        if (empty($message['filter']) && empty($message['touser'])) {
            throw new RuntimeException('The message reception object is not specified');
        }

        $api = Arr::get($message, 'filter.is_to_all') ? 'cgi-bin/message/mass/sendall' : 'cgi-bin/message/mass/send';
        return $this->httpPostJson($api, $message);
    }

    /**
     * 预览一个消息
     *
     * @param array $message
     * @return mixed
     */
    public function preview(array $message)
    {
        return $this->httpPostJson('cgi-bin/message/mass/preview', $message);
    }

    /**
     * 删除一个消息
     *
     * @param string $msgId
     * @return mixed
     */
    public function delete(string $msgId)
    {
        $options = [
            'msg_id' => $msgId,
        ];

        return $this->httpPostJson('cgi-bin/message/mass/delete', $options);
    }

    /**
     * 状态
     *
     * @param string $msgId
     * @return mixed
     */
    public function status(string $msgId)
    {
        $options = [
            'msg_id' => $msgId,
        ];

        return $this->httpPostJson('cgi-bin/message/mass/get', $options);
    }

    /**
     * 发送文本消息
     *
     * @param string $message
     * @param null $to
     * @return array|object|\Psr\Http\Message\ResponseInterface|\Sinta\Wechat\Kernel\Support\Collection|string
     */
    public function sendText(string $message, $to = null)
    {
        return $this->sendMessage(new Text($message), $to);
    }


    public function sendNews(string $mediaId,$to = null)
    {
        return $this->sendMessage(new Media($mediaId, 'mpnews'), $to);
    }

    public function sendVoice(string $mediaId, $to = null)
    {
        return $this->sendMessage(new Media($mediaId, 'voice'), $to);
    }


    public function sendImage(string $mediaId, $to = null)
    {
        return $this->sendMessage(new Image($mediaId), $to);
    }

    public function sendVideo(string $mediaId, $to = null)
    {
        return $this->sendMessage(new Media($mediaId, 'mpvideo'), $to);
    }

    public function sendCard(string $cardId, $to = null)
    {
        return $this->sendMessage(new Card($cardId), $to);
    }

    public function previewText(string $message, $to, $by = self::PREVIEW_BY_OPENID)
    {
        return $this->previewMessage(new Text($message), $to, $by);
    }

    public function previewNews(string $mediaId, $to, $by = self::PREVIEW_BY_OPENID)
    {
        return $this->previewMessage(new Media($mediaId, 'mpnews'), $to, $by);
    }

    public function previewVoice(string $mediaId, $to, $by = self::PREVIEW_BY_OPENID)
    {
        return $this->previewMessage(new Media($mediaId, 'voice'), $to, $by);
    }

    public function previewImage(string $mediaId, $to, $by = self::PREVIEW_BY_OPENID)
    {
        return $this->previewMessage(new Image($mediaId), $to, $by);
    }

    public function previewVideo(string $mediaId, $reception, $method = self::PREVIEW_BY_OPENID)
    {
        return $this->previewMessage(new Media($mediaId, 'mpvideo'), $reception, $method);
    }

    public function previewCard(string $cardId, $reception, $method = self::PREVIEW_BY_OPENID)
    {
        return $this->previewMessage(new Card($cardId), $reception, $method);
    }

    public function previewMessage(MessageInterface $message, string $reception, $method = self::PREVIEW_BY_OPENID)
    {
        $message = (new MessageBuilder())->message($message)->buildForPreview($method, $reception);

        return $this->preview($message);
    }

    public function sendMessage(MessageInterface $message, $reception = null)
    {
        $message = (new MessageBuilder())->message($message)->toAll();
        if (is_int($reception)) {
            $message->toTag($reception);
        } elseif (is_array($reception)) {
            $message->toUsers($reception);
        }
        return $this->send($message->build());
    }

    
    public function __call($method, $args)
    {
        if (strpos($method, 'ByName') > 0) {
            $method = strstr($method, 'ByName', true);

            if (method_exists($this, $method)) {
                array_push($args, self::PREVIEW_BY_NAME);

                return $this->$method(...$args);
            }
        }

        throw new \BadMethodCallException(sprintf('Method %s not exists.', $method));
    }
}