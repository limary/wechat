<?php
namespace Sinta\Wechat\Kernel;

use Sinta\Wechat\Kernel\Contracts\MessageInterface;
use Sinta\Wechat\Kernel\Exceptions\BadRequestException;
use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;
use Sinta\Wechat\Kernel\Messages\News;
use Sinta\Wechat\Kernel\Messages\NewsItem;
use Sinta\Wechat\Kernel\Messages\Raw as RawMessage;
use Sinta\Wechat\Kernel\Messages\Text;
use Sinta\Wechat\Kernel\Support\XML;
use Sinta\Wechat\Kernel\Messages\Message;
use Sinta\Wechat\Kernel\Traits\Observable;
use Sinta\Wechat\Kernel\Traits\ResponseCastable;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 * 1. url 里的 signature 只是将 token+nonce+timestamp 得到的签名，只是用于验证当前请求的，在公众号环境下一直有
 * 2. 企业号消息发送时是没有的，因为固定为完全模式，所以 url 里不会存在 signature, 只有 msg_signature 用于解密消息的
 *
 * Class ServerGuard
 * @package Sinta\Wechat\Kernel
 */
class ServerGuard
{
    use Observable,ResponseCastable;

    /**
     * @var bool
     */
    protected $alwaysValidate = false;

    const SUCCESS_EMPTY_RESPONSE = 'success';

    const MESSAGE_TYPE_MAPPING = [
        'text' => Message::TEXT,
        'image' => Message::IMAGE,
        'voice' => Message::VOICE,
        'video' => Message::VIDEO,
        'shortvideo' => Message::SHORT_VIDEO,
        'location' => Message::LOCATION,
        'link' => Message::LINK,
        'device_event' => Message::DEVICE_EVENT,
        'device_text' => Message::DEVICE_TEXT,
        'event' => Message::EVENT,
        'file' => Message::FILE,
        'miniprogrampage' => Message::MINIPROGRAM_PAGE,
    ];

    /**
     * @var \Sinta\Wechat\Kernel\ServiceContainer
     */
    protected $app;


    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;

        foreach ($this->app->extension->observers() as $observer) {
            call_user_func_array([$this, 'push'], $observer);
        }
    }


    public function serve(): Response
    {
        $this->app['logger']->debug('Request received:', [
            'method' => $this->app['request']->getMethod(),
            'uri' => $this->app['request']->getUri(),
            'content-type' => $this->app['request']->getContentType(),
            'content' => $this->app['request']->getContent(),
        ]);

        $response = $this->validate()->resolve();

        $this->app['logger']->debug('Server response created:', ['content' => $response->getContent()]);

        return $response;
    }


    /**
     * 验证
     *
     * @return $this
     * @throws BadRequestException
     */
    public function validate()
    {
        if (!$this->isSafeMode()) {
            return $this;
        }

        if ($this->app['request']->get('signature') !== $this->signature([
                $this->getToken(),
                $this->app['request']->get('timestamp'),
                $this->app['request']->get('nonce'),
            ])) {
            throw new BadRequestException('Invalid request signature.', 400);
        }

        return $this;
    }


    public function forceValidate()
    {
        $this->alwaysValidate = true;
        return $this;
    }


    /**
     * 获取消息
     *
     * @return array|mixed|null
     * @throws BadRequestException
     */
    public function getMessage()
    {
        $message = $this->parseMessage($this->app['request']->getContent(false));

        if (!is_array($message) || empty($message)) {
            throw new BadRequestException('No message received.');
        }

        if ($this->isSafeMode() && !empty($message['Encrypt'])) {
            $message = $this->app['encryptor']->decrypt(
                $message['Encrypt'],
                $this->app['request']->get('msg_signature'),
                $this->app['request']->get('nonce'),
                $this->app['request']->get('timestamp')
            );

            // Handle JSON format.
            $dataSet = json_decode($message, true);

            if ($dataSet && (JSON_ERROR_NONE === json_last_error())) {
                return $dataSet;
            }

            return XML::parse($message);
        }

        return $message;
    }

    /**
     * 处理
     *
     * @return Response
     */
    protected function resolve(): Response
    {
        $result = $this->handleRequest();

        if ($this->shouldReturnRawResponse()) {
            return new Response($result['response']);
        }

        return new Response(
            $this->buildResponse($result['to'], $result['from'], $result['response']),
            200,
            ['Content-Type' => 'application/xml']
        );
    }

    protected function getToken()
    {
        return $this->app['config']['token'];
    }

    /**
     * 建立响应
     *
     * @param string $to
     * @param string $from
     * @param $message
     * @return mixed|string
     * @throws InvalidArgumentException
     */
    public function buildResponse(string $to, string $from, $message)
    {
        if (empty($message) || $message === self::SUCCESS_EMPTY_RESPONSE) {
            return self::SUCCESS_EMPTY_RESPONSE;
        }

        if ($message instanceof RawMessage) {
            return $message->get('content', self::SUCCESS_EMPTY_RESPONSE);
        }

        if (is_string($message) || is_numeric($message)) {
            $message = new Text((string) $message);
        }

        if (is_array($message) && reset($message) instanceof NewsItem) {
            $message = new News($message);
        }

        if (!($message instanceof Message)) {
            throw new InvalidArgumentException(sprintf('Invalid Messages type "%s".', gettype($message)));
        }

        return $this->buildReply($to, $from, $message);
    }

    /**
     * 处理响应
     *
     * @return array
     */
    protected function handleRequest(): array
    {
        $castedMessage = $this->getMessage();

        $messageArray = $this->detectAndCastResponseToType($castedMessage, 'array');

        $response = $this->dispatch(self::MESSAGE_TYPE_MAPPING[$messageArray['MsgType'] ?? $messageArray['msg_type'] ?? 'text'], $castedMessage);

        return [
            'to' => $messageArray['FromUserName'] ?? '',
            'from' => $messageArray['ToUserName'] ?? '',
            'response' => $response,
        ];
    }


    /**
     * 创建应答
     *
     * @param string $to
     * @param string $from
     * @param MessageInterface $message
     * @return string
     */
    protected function buildReply(string $to, string $from, MessageInterface $message): string
    {
        $prepends = [
            'ToUserName' => $to,
            'FromUserName' => $from,
            'CreateTime' => time(),
            'MsgType' => $message->getType(),
        ];

        $response = $message->transformToXml($prepends);

        if ($this->isSafeMode()) {
            $this->app['logger']->debug('Messages safe mode is enabled.');
            $response = $this->app['encryptor']->encrypt($response);
        }

        return $response;
    }


    /**
     * 产生签名
     *
     * @param array $params
     * @return string
     */
    protected function signature(array $params)
    {
        sort($params, SORT_STRING);

        return sha1(implode($params));
    }

    /**
     * 解析消息
     *
     * @param $content
     * @return array
     * @throws BadRequestException
     */
    protected function parseMessage($content)
    {
        try {
            if (stripos($content, '<') === 0) {
                $content = XML::parse($content);
            } else {
                // Handle JSON format.
                $dataSet = json_decode($content, true);
                if ($dataSet && (JSON_ERROR_NONE === json_last_error())) {
                    $content = $dataSet;
                }
            }

            return (array) $content;
        } catch (\Exception $e) {
            throw new BadRequestException(sprintf('Invalid message content:(%s) %s', $e->getCode(), $e->getMessage()), $e->getCode());
        }
    }


    /**
     * 是否是安全模式
     *
     * @return bool
     */
    protected function isSafeMode(): bool
    {
        return $this->app['request']->get('signature') && 'aes' === $this->app['request']->get('encrypt_type');
    }

    /**
     * 是否返回原响应
     *
     * @return bool
     */
    protected function shouldReturnRawResponse(): bool
    {
        return false;
    }

    protected function decryptMessage(array $message)
    {
        return $message = $this->app['encryptor']->decrypt(
            $message['Encrypt'],
            $this->app['request']->get('msg_signature'),
            $this->app['request']->get('nonce'),
            $this->app['request']->get('timestamp')
        );
    }
}