<?php
namespace Sinta\Wechat\Work\User;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    /**
     * 创建成员
     *
     *
     * userid	是	成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
        name	是	成员名称。长度为1~64个字节
        department	是	成员所属部门id列表,不超过20个
        position	否	职位信息。长度为0~64个字节
        mobile	否	手机号码。企业内必须唯一，mobile/weixinid/email三者不能同时为空
        gender	否	性别。1表示男性，2表示女性
        email	否	邮箱。长度为0~64个字节。企业内必须唯一
        weixinid	否	微信号。企业内必须唯一。（注意：是微信号，不是微信的名字）
        avatar_mediaid	否	成员头像的mediaid，通过多媒体接口上传图片获得的mediaid
        extattr	否	扩展属性。扩展属性需要在WEB管理端创建后才生效，否则忽略未知属性的赋值
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @http://qydev.weixin.qq.com/wiki/index.php?title=%E7%AE%A1%E7%90%86%E6%88%90%E5%91%98
     */
    public function create(array $data)
    {
        return $this->httpPostJson('cgi-bin/user/create', $data);
    }

    /**
     * 更新成员
     *
     * @param string $id
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update(string $id, array $data)
    {
        return $this->httpPostJson('cgi-bin/user/update', array_merge(['userid' => $id], $data));
    }

    /**
     * 删除成员
     *
     * @param $userId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete($userId)
    {
        if (is_array($userId)) {
            return $this->batchDelete($userId);
        }
        return $this->httpGet('cgi-bin/user/delete', ['userid' => $userId]);
    }

    /**
     * 批量删除成员
     *
     * @param array $userIds
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function batchDelete(array $userIds)
    {
        return $this->httpPost('cgi-bin/user/batchdelete', ['useridlist' => $userIds]);
    }

    /**
     * 获取成员
     *
     * @param string $userId 对应管理端的帐号
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get(string $userId)
    {
        return $this->httpGet('cgi-bin/user/get', ['userid' => $userId]);
    }

    /**
     * 获取部门成员
     *
     * @param int $departmentId
     * @param bool $fetchChild
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getDepartmentUsers(int $departmentId, bool $fetchChild = false)
    {
        $params = [
            'department_id' => $departmentId,
            'fetch_child' => (int) $fetchChild,
        ];
        return $this->httpGet('cgi-bin/user/simplelist', $params);
    }

    /**
     * 获取部门成员(详情)
     *
     * @param int $departmentId 获取的部门id
     * @param bool $fetchChild  是否递归获取子部门下面的成员
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getDetailedDepartmentUsers(int $departmentId, bool $fetchChild = false)
    {
        $params = [
            'department_id' => $departmentId,
            'fetch_child' => (int) $fetchChild,
        ];
        return $this->httpGet('cgi-bin/user/list', $params);
    }

    /**
     * userid转换成openid接口
     *
     * 该接口使用场景为微信支付、微信红包和企业转账，企业号用户在使用微信支付的功能时，需要自行将企业号的userid转成openid。
     * 在使用微信红包功能时，需要将应用id和userid转成appid和openid才能使用。
     *
     * @param string $userId       企业号内的成员id
     * @param int|null $agentId 整型，需要发送红包的应用ID，若只是使用微信支付和企业转账，则无需该参数
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function userIdToOpenid(string $userId, int $agentId = null)
    {
        $params = [
            'userid' => $userId,
            'agentid' => $agentId,
        ];
        return $this->httpPostJson('cgi-bin/user/convert_to_openid', $params);
    }

    /**
     * openid转换成userid接口
     *
     * 该接口主要应用于使用微信支付、微信红包和企业转账之后的结果查询，开发者需要知道某个结果事件的openid对应企业号内成员的信息时，
     * 可以通过调用该接口进行转换查询。
     *
     * @param string $openid  在使用微信支付、微信红包和企业转账之后，返回结果的openid
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function openidToUserId(string $openid)
    {
        $params = [
            'openid' => $openid,
        ];
        return $this->httpPostJson('cgi-bin/user/convert_to_userid', $params);
    }

    /**
     * 成员关注企业号
     *
     * @param string $userId 成员UserID
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function accept(string $userId)
    {
        $params = [
            'userid' => $userId,
        ];
        return $this->httpGet('cgi-bin/user/authsucc', $params);
    }


}