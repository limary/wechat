<?php
namespace Sinta\Wechat\OfficialAccount\ShakeAround;

use Sinta\Wechat\Kernel\Client as BaseClient;

class DeviceClient extends BaseClient
{
    public function apply(array $data)
    {
        return $this->httpPostJson('shakearound/device/applyid',$data);
    }


    public function status(int $applyId)
    {
        $params = [
            'apply_id' => $applyId,
        ];

        return $this->httpPostJson('shakearound/device/applystatus', $params);
    }


    public function update(array $deviceIdentifier, string $comment)
    {
        $params = [
            'device_identifier' => $deviceIdentifier,
            'comment' => $comment,
        ];

        return $this->httpPostJson('shakearound/device/update', $params);
    }

    public function bindPoi(array $deviceIdentifier, int $poiId)
    {
        $params = [
            'device_identifier' => $deviceIdentifier,
            'poi_id' => $poiId,
        ];

        return $this->httpPostJson('shakearound/device/bindlocation', $params);
    }


    public function bindThirdPoi(array $deviceIdentifier, int $poiId, string $appId)
    {
        $params = [
            'device_identifier' => $deviceIdentifier,
            'poi_id' => $poiId,
            'type' => 2,
            'poi_appid' => $appId,
        ];

        return $this->httpPostJson('shakearound/device/bindlocation', $params);
    }

    public function listByIds(array $deviceIdentifiers)
    {
        $params = [
            'type' => 1,
            'device_identifiers' => $deviceIdentifiers,
        ];

        return $this->search($params);
    }

    public function list(int $lastId, int $count)
    {
        $params = [
            'type' => 2,
            'last_seen' => $lastId,
            'count' => $count,
        ];

        return $this->search($params);
    }


    public function listByApplyId(int $applyId, int $lastId, int $count)
    {
        $params = [
            'type' => 3,
            'apply_id' => $applyId,
            'last_seen' => $lastId,
            'count' => $count,
        ];

        return $this->search($params);
    }


    public function search(array $params)
    {
        return $this->httpPostJson('shakearound/device/search', $params);
    }
}