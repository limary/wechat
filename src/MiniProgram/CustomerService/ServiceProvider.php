<?php
namespace Sinta\Wechat\MiniProgram\CustomerService;

use Sinta\Wechat\OfficialAccount\CustomerService\Client;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 客服服务
 *
 * Class ServiceProvider.
 *
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['customer_service'] = function ($app) {
            return new Client($app);
        };
    }
}
