<?php
namespace Sinta\Wechat\Kernel\Messages;


use Sinta\Wechat\Kernel\Contracts\MediaInterface;
use Sinta\Wechat\Kernel\Support\Str;

class Media extends Message implements MediaInterface
{
    /**
     * 属性
     *
     * @var array
     */
    protected $properties = ['media_id'];


    protected $required = [
        'media_id',
    ];

    public function __construct(string $mediaId, $type = null, array $attributes = [])
    {
        parent::__construct(array_merge(['media_id' => $mediaId], $attributes));

        !empty($type) && $this->setType($type);
    }


    public function getMediaId(): string
    {
        $this->checkRequiredAttributes();

        return $this->get('media_id');
    }

    public function toXmlArray()
    {
        return [
            Str::studly($this->getType()) => [
                'MediaId' => $this->get('media_id'),
            ],
        ];
    }
}