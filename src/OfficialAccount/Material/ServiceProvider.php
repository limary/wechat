<?php
namespace Sinta\Wechat\OfficialAccount\Material;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['material'] = function($app){
            return new Client($app);
        };
    }
}