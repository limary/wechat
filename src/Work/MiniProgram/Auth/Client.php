<?php
namespace Sinta\Wechat\Work\MiniProgram\Auth;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    public function session(string $code)
    {
        $params = [
            'js_code' => $code,
            'grant_type' => 'authorization_code',
        ];
        return $this->httpGet('cgi-bin/miniprogram/jscode2session', $params);
    }
}