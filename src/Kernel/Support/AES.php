<?php
namespace Sinta\Wechat\Kernel\Support;


class AES
{
    /**
     * 加密处理
     *
     * @param string $text
     * @param string $key
     * @param string $iv
     * @param int $option
     * @return string
     */
    public static function encrypt(string $text, string $key, string $iv, int $option = OPENSSL_RAW_DATA): string
    {
        self::validateKey($key);
        self::validateIv($iv);

        return openssl_encrypt($text, self::getMode($key), $key, $option, $iv);
    }

    /**
     * 解密处理
     *
     * @param string $cipherText
     * @param string $key
     * @param string $iv
     * @param int $option
     * @return string
     */
    public static function decrypt(string $cipherText, string $key, string $iv, int $option = OPENSSL_RAW_DATA, $method = null): string
    {
        self::validateKey($key);
        self::validateIv($iv);

        return openssl_decrypt($cipherText, $method ?: self::getMode($key), $key, $option, $iv);
    }

    public static function getMode($key)
    {
        return 'aes-'.(8 * strlen($key)).'-cbc';
    }


    public static function validateKey(string $key)
    {
        if (!in_array(strlen($key), [16, 24, 32], true)) {
            throw new \InvalidArgumentException(sprintf('Key length must be 16, 24, or 32 bytes; got key len (%s).', strlen($key)));
        }
    }

    public static function validateIv(string $iv)
    {
        if (strlen($iv) !== 16) {
            throw new \InvalidArgumentException('IV length must be 16 bytes.');
        }
    }
}