<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\OfficialAccount;

use Sinta\Wechat\OpenPlatform\Application;
use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\OpenPlatform\Authorizer\Aggregate\Account\Client as BaseClient;

class Client extends BaseClient
{
    /**
     * @var Application
     */
    protected $component;


    public function __construct(ServiceContainer $app, Application $component)
    {
        parent::__construct($app);
        $this->component = $component;
    }

    /**
     * 从第三方平台跳转至微信公众平台授权注册页面, 授权注册小程序.
     *
     * @param string $callbackUrl
     * @param bool $copyWxVerify
     * @return string
     */
    public function getFastRegistrationUrl(string $callbackUrl, bool $copyWxVerify = true): string
    {
        $queries = [
            'copy_wx_verify' => $copyWxVerify,
            'component_appid' => $this->component['config']['app_id'],
            'appid' => $this->app['config']['app_id'],
            'redirect_uri' => $callbackUrl,
        ];
        return 'https://mp.weixin.qq.com/cgi-bin/fastregisterauth?'.http_build_query($queries);
    }


    public function register(string $ticket)
    {
        $params = [
            'ticket' => $ticket,
        ];
        return $this->httpPostJson('cgi-bin/account/fastregister', $params);
    }
}