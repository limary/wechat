<?php
namespace Sinta\Wechat\Kernel\Contracts;

/**
 * 消息接口
 *
 * Interface IMessage
 * @package Sinta\Wechat\Kernel\Contract
 */
interface MessageInterface
{
    /**
     * 消息类型
     *
     * @return string
     */
    public function getType():string;


    /**
     *
     *
     * @return array
     */
    public function transformForJsonRequest():array;


    /**
     * 转换成XML
     *
     * @return string
     */
    public function transformToXml(): string;

}