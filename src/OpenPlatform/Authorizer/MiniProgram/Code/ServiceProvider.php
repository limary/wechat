<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Code;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 代码管理
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Code
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['code'] = function ($app){
            return new Client($app);
        };
    }
}