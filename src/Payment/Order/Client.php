<?php

namespace Sinta\Wechat\Payment\Order;

use Sinta\Wechat\Kernel\Support;
use Sinta\Wechat\Payment\Kernel\BaseClient;

/**
 * 订单相关的方法
 *
 * Class Client
 * @package Sinta\Wechat\Payment\Order
 */
class Client extends BaseClient
{
    /**
     * 通一下订单
     *
     * @param array $order
     * @see https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_1
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function unify(array $attributes)
    {
        if (empty($attributes['spbill_create_ip'])) {
            $attributes['spbill_create_ip'] = ($attributes['trade_type'] === 'NATIVE') ?
                Support\get_server_ip() : Support\get_client_ip();
        }
        $attributes['appid'] = $this->app['config']->app_id;
        $attributes['notify_url'] = $attributes['notify_url'] ?? $this->app['config']['notify_url'];

        return $this->request('pay/unifiedorder', $attributes);
    }


    /**
     * 查询订单
     *
     * @param array $params
     * @see https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_2
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function query(array $params)
    {
        $params['appid'] = $this->app['config']->app_id;

        return $this->request('pay/orderquery',$params);
    }

    /**
     * 关闭订单
     *
     * @param string $tradeNo
     * @see https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_3
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function close(string $tradeNo)
    {
        $params = [
            'out_trade_no' => $tradeNo,
            'appid' => $this->app['config']->app_id,
        ];

        return $this->request('pay/closeorder', $params);
    }

    /**
     * 通过交号查询订单
     *
     * @param string $transactionId
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|Client
     */
    public function queryByTransactionId(string $transactionId)
    {
        return $this->query([
            'transaction_id' => $transactionId,
        ]);
    }

    /**
     * 通过订单号查询订单
     *
     * @param string $number
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|Client
     */
    public function queryByOutTradeNumber(string $number)
    {
        return $this->query([
            'out_trade_no' => $number,
        ]);
    }

}