<?php
namespace Sinta\Wechat\OpenPlatform\Auth;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['verify_ticket'] = function($app){
            return new VerifyTicket($app['config']['app_id']);
        };

        $app['access_token'] = function ($app){
            return new AccessToken($app);
        };
    }
}