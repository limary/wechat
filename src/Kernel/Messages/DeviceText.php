<?php
namespace Sinta\Wechat\Kernel\Messages;


class DeviceText extends Message
{
    protected $type = 'device_text';

    protected $properties = [
        'device_type',
        'device_id',
        'content',
        'session_id',
        'open_id',
    ];

    public function toXmlArray()
    {
        return [
            'DeviceType' => $this->get('device_type'),
            'DeviceID' => $this->get('device_id'),
            'SessionID' => $this->get('session_id'),
            'Content' => base64_encode($this->get('content')),
        ];
    }
}