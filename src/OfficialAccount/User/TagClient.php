<?php
namespace Sinta\Wechat\OfficialAccount\User;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * Class TagClient
 * @package Sinta\Wechat\OfficialAccount\User
 * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837
 */
class TagClient extends BaseClient
{
    /**
     * 用户创建标签
     *
     * 一个公众号，最多可以创建100个标签。
     *
     * @param string $name
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(string $name)
    {
        $params = [
            'tag' => ['name' => $name]
        ];

        return $this->httpPostJson('cgi-bin/tag/create',$params);
    }

    /**
     * 获取公众号已创建的标签
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list()
    {
        return $this->httpGet('cgi-bin/tags/get');
    }

    /**
     * 修改标签
     *
     * @param int $tagId
     * @param string $name
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update(int $tagId,string $name)
    {
        $params = [
            'tag' => [
                'id' => $tagId,
                'name' => $name,
            ],
        ];

        return $this->httpPostJson('cgi-bin/tags/update',$params);
    }

    /**
     * 删除标签
     *
     * @param int $tagId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(int $tagId)
    {
        $params = [
            'tag' => ['id' => $tagId],
        ];
        return $this->httpPostJson('cgi-bin/tags/delete', $params);
    }

    /**
     *
     *
     * @param string $openid
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function userTags(string $openid)
    {
        $params = ['openid' => $openid];
        return $this->httpPostJson('cgi-bin/tags/getidlist', $params);
    }

    /**
     * 获取标签下粉丝列表
     *
     * @param int $tagId
     * @param string $nextOpenId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function usersOfTag(int $tagId, string $nextOpenId = '')
    {
        $params = [
            'tagid' => $tagId,
            'next_openid' => $nextOpenId,
        ];
        return $this->httpPostJson('cgi-bin/user/tag/get', $params);
    }

    /**
     * 批量给用户打上标签
     *
     * @param array $openids
     * @param int $tagId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function tagUsers(array $openids, int $tagId)
    {
        $params = [
            'openid_list' => $openids,
            'tagid' => $tagId,
        ];
        return $this->httpPostJson('cgi-bin/tags/members/batchtagging', $params);
    }

    /**
     * 批量为用户取消标签
     *
     * @param array $openids
     * @param int $tagId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function untagUsers(array $openids, int $tagId)
    {
        $params = [
            'openid_list' => $openids,
            'tagid' => $tagId,
        ];
        return $this->httpPostJson('cgi-bin/tags/members/batchuntagging', $params);
    }


}