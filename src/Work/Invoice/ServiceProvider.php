<?php
namespace Sinta\Wechat\Work\Invoice;

use Pimple\Container;
use Pimple\ServiceProviderInterface;


class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['invoice'] = function ($app) {
            return new Client($app);
        };
    }
}