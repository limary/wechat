<?php
namespace Sinta\Wechat\OpenWork\Corp;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider
{
    public function register(Container $app)
    {
        isset($app['corp']) || $app['corp'] = function ($app) {
            return new Client($app);
        };
    }
}