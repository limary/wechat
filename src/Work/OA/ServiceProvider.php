<?php
namespace Sinta\Wechat\Work\OA;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['oa'] = function ($app){
            return new Client($app);
        };
    }
}