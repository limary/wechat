<?php
namespace Sinta\Wechat\BasicService\Jssdk;

use Sinta\Wechat\Kernel\Support;
use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Traits\InteractsWithCache;
use Sinta\Wechat\Kernel\Exceptions\RuntimeException;


class Client extends BaseClient
{
    use InteractsWithCache;

    protected $ticketEndpoint = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket';

    protected $url;


    /**
     * 获取果断设置json
     *
     * @param array $jsApiList
     * @param bool $debug
     * @param bool $beta
     * @param bool $json
     * @return array|string
     */
    public function buildConfig(array $jsApiList, bool $debug = false, bool $beta = false, bool $json = true)
    {
        $config = array_merge(compact('debug', 'beta', 'jsApiList'), $this->signature());

        return $json ? json_encode($config) : $config;
    }

    public function getConfigArray(array $apis, bool $debug = false, bool $beta = false)
    {
        return $this->buildConfig($apis, $debug, $beta, false);
    }



    public function getTicket(bool $refresh = false, string $type = 'jsapi'): array
    {
        $cacheKey = sprintf('sinta-wechat.basic_service.jssdk.ticket.%s.%s', $type, $this->getAppId());

        if (!$refresh && $this->getCache()->has($cacheKey)) {
            return $this->getCache()->get($cacheKey);
        }

        $result = $this->resolveResponse(
            $this->requestRaw($this->ticketEndpoint, 'GET', ['query' => ['type' => $type]]),
            'array'
        );

        $this->getCache()->set($cacheKey, $result, $result['expires_in'] - 500);

        if (!$this->getCache()->has($cacheKey)) {
            throw new RuntimeException('Failed to cache jssdk ticket.');
        }

        return $result;
    }

    /**
     * 创建签名
     *
     * @param string|null $url
     * @param string|null $nonce
     * @param null $timestamp
     * @return array
     */
    protected function signature(string $url = null, string $nonce = null, $timestamp = null): array
    {
        $url = $url ?: $this->getUrl();
        $nonce = $nonce ?: Support\Str::quickRandom(10);
        $timestamp = $timestamp ?: time();

        return [
            'appId' => $this->getAppId(),
            'nonceStr' => $nonce,
            'timestamp' => $timestamp,
            'url' => $url,
            'signature' => $this->getTicketSignature($this->getTicket()['ticket'], $nonce, $timestamp, $url),
        ];
    }

    public function getTicketSignature($ticket, $nonce, $timestamp, $url): string
    {
        return sha1(sprintf('jsapi_ticket=%s&noncestr=%s&timestamp=%s&url=%s', $ticket, $nonce, $timestamp, $url));
    }

    public function dictionaryOrderSignature()
    {
        $params = func_get_args();
        sort($params, SORT_STRING);
        return sha1(implode('', $params));
    }

    public function setUrl(string $url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get current url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        if ($this->url) {
            return $this->url;
        }

        return Support\current_url();
    }


    public function getAppId()
    {
        return $this->app['config']->get('app_id');
    }
}