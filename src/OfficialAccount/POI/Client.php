<?php
namespace Sinta\Wechat\OfficialAccount\POI;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 门店服务
 *
 * Class Client
 * @package Sinta\Wechat\OfficialAccount\POI
 */
class Client extends BaseClient
{

    /**
     * 获取门店支持的分类
     *
     * @return mixed
     */
    public function categories()
    {
        return $this->httpGet('cgi-bin/poi/getwxcategory');
    }

    /**
     * 获取门店通过门店ID
     *
     * @param int $poiId
     */
    public function get(int $poiId)
    {
        return $this->httpPostJson('cgi-bin/poi/getpoi', ['poi_id' => $poiId]);
    }


    /**
     * 获取门店列表
     *
     * @param int $offset
     * @param int $limit
     * @return mixed
     */
    public function list(int $offset = 0, int $limit = 10)
    {
        $params = [
            'begin' => $offset,
            'limit' => $limit,
        ];

        return $this->httpPostJson('cgi-bin/poi/getpoilist', $params);
    }

    /**
     * 创建门店
     *
     * @param array $baseInfo
     * @return mixed
     */
    public function create(array $baseInfo)
    {
        $params = [
            'business' => [
                'base_info' => $baseInfo,
            ],
        ];

        return $this->httpPostJson('cgi-bin/poi/addpoi', $params);
    }


    public function createAndGetId(array $databaseInfo)
    {
        return $this->create($databaseInfo)['poi_id'];
    }

    /**
     * 更新门店
     *
     * @param int $poiId
     * @param array $baseInfo
     * @return mixed
     */
    public function update(int $poiId, array $baseInfo)
    {
        $params = [
            'business' => [
                'base_info' => array_merge($baseInfo, ['poi_id' => $poiId]),
            ],
        ];

        return $this->httpPostJson('cgi-bin/poi/updatepoi', $params);
    }


    /**
     * 删除一个门店
     *
     * @param int $poiId
     * @return mixed
     */
    public function delete(int $poiId)
    {
        return $this->httpPostJson('cgi-bin/poi/delpoi', ['poi_id' => $poiId]);
    }
}